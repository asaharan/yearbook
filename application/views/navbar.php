<nav style="background: #2196F3;">
	<div class="nav-wrapper">
		<ul id="nav-mobile" class="left">
			<li><a href="<?php echo base_url(); ?>">Home</a></li>
			<li><a href="<?php echo base_url(); ?>me">Your Profile</a></li>
			<li><a href="<?php echo base_url(); ?>explore">Find Friends</a></li>
			<li style="background: rgba(255,255,255,0.15);"><a href="<?php echo base_url(); ?>gallery">Gallery<sup style="margin-left: 3px;font-size: 10px;top: -0.9em;">New</sup></a></li>
		</ul>
		<ul class="right">			
			<li><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>
		</ul>
	</div>
</nav>