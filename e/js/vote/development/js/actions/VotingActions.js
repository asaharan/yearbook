var AppDispatcher = require('../dispatcher/AppDispatcher');
var Constants = require('../constants/Constants');

var VotingActions={
	init:function(){
		AppDispatcher.dispatch({
			actionType:Constants.INIT
		});
	},
	updateStats:function(){
		AppDispatcher.dispatch({
			actionType:Constants.UPDATE_STATS
		});
	},
	voteFor:function(id,clear){
		AppDispatcher.dispatch({
			actionType:Constants.VOTE_FOR,
			id:id,
			clear:clear,
		});
	},
	searchBoxFocused:function(state){
		AppDispatcher.dispatch({
			actionType:Constants.SEARCHBOX_FOCUSED,
			state:state
		});
	},
	searchQueryChanged:function(q){
		AppDispatcher.dispatch({
			actionType:Constants.SEARCHQUERY_CHANGED,
			q:q
		});
	},
	updateShowAllFlag:function(flag){
		AppDispatcher.dispatch({
			actionType:Constants.UPDATE_SHOWALLFLAG,
			flag:flag
		});
	}
};
module.exports=VotingActions;