<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function is_logged_in(){
	$CI=&get_instance();
	$user_id = $CI->input->cookie('user_id');
	$token = $CI->input->cookie('token');
	$r=$CI->db->get_where('login_history',array('user_id'=>$user_id,'token'=>$token,'valid'=>1));
	echo json_encode($r);
	if(sizeof($r)>0){
		return true;
	}
	return false;
}