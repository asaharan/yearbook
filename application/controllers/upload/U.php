<?php

class U extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	private function upload_directory($photo_base){
		$r= realpath(dirname(__FILE__));
		// echo $r;
		$b=explode('/', $r);
		// echo sizeof($b);
		$x='';
		for($i=0;$i<sizeof($b)-3;$i++){
			$x.=$b[$i].'/';
		}
		$x.=$photo_base;
		return $x;
	}
	function indexify()
	{
		$this->load->view('upload/form', array('error' => ' ' ));
	}
	function delete(){
		$this->load->helper('login');
		if(!isloggedin()){
			redirect('/gallery','refresh');
			return;
		}
		$userid=userid();
		$id=$this->input->get('i');
		$this->db->where(array('id'=>$id,'user_id'=>$userid));
		$this->db->update('gallery',array('deleted'=>1));
		redirect('/gallery','refresh');
	}
	function do_upload()
	{
		$this->load->helper('login');
		$response=[];
		$response['success']=FALSE;
		$pass = $this->input->post('p');
		$adminPass = $this->input->post('ap');
		$username = $this->input->post('u');
		if(!isloggedin()){
			return;
		}
		$photo_base='e/img/f/';
		$userid=userid();
		$config['upload_path'] = $this->upload_directory($photo_base);
		$config['allowed_types'] = '*';
		$config['max_size']	= '204800';
		$config['file_name']=generateRandomString().'-'.$userid.'.jpg';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			// echo $error['error'];
			// echo json_encode($error);
			$response['error']='Some error occurred';
			$response['error']=$error['error'];
			if($error['error']=='<p>The uploaded file exceeds the maximum allowed size in your PHP configuration file.</p>'){
				$response['error']='File too large';
			}
			echo json_encode($response);
		}
		else
		{
			$response['success']=TRUE;
			$data = array('upload_data' => $this->upload->data());
			$file_name=$data['upload_data']['file_name'];
			$response['avatar']=$photo_base.$file_name;
			$this->db->insert('gallery',array('user_id'=>userid(),'path'=>$photo_base.$file_name));
			redirect('/gallery','refresh');
			return;
		}
	}
}
?>