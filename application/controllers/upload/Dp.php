<?php

class Dp extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}
	private function upload_directory($photo_base){
		$r= realpath(dirname(__FILE__));
		// echo $r;
		$b=explode('/', $r);
		// echo sizeof($b);
		$x='';
		for($i=0;$i<sizeof($b)-3;$i++){
			$x.=$b[$i].'/';
		}
		$x.=$photo_base;
		return $x;
	}
	function index()
	{
		$this->load->view('upload/form', array('error' => ' ' ));
	}

	function do_upload()
	{
		$response=[];
		$response['success']=FALSE;
		$photo_base='e/img/u/';
		if($this->input->get('preview')!='no'){
			$photo_base.='t/';
		}
		$this->load->helper('login');
		if(!isloggedin()){
			echo "You are not logged in.";
			die();
		}else{
		}
		$userid=userid();
		$config['upload_path'] = $this->upload_directory($photo_base);
		$config['allowed_types'] = '*';
		$config['max_size']	= '20480';
		$config['file_name']=$userid.'_x.jpg';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			// echo $error['error'];
			// echo json_encode($error);
			$response['error']='Some error occurred';
			$response['error']=$error['error'];
			if($error['error']=='<p>The uploaded file exceeds the maximum allowed size in your PHP configuration file.</p>'){
				$response['error']='File too large';
			}
			echo json_encode($response);
		}
		else
		{
			list($width, $height, $type, $attr) = getimagesize($_FILES["userfile"]['tmp_name']);
			$response['success']=TRUE;
			$data = array('upload_data' => $this->upload->data());
			$file_name=$data['upload_data']['file_name'];
			
			if( ( $this->input->get('preview')!='no' && $height > 500 ) || ( $this->input->get('preview')=='no' && $height>200 ) ){
				$config['source_image']=$config['upload_path'].$file_name;
				$config['height'] = 500;
				if($this->input->get('preview')=='no'){
					$config['height']=200;
				}
				$new_file_name=str_replace('.jpg', 'opt.jpg', $file_name);
				if($this->input->get('preview')=='no'){
					$new_file_name=$file_name;
				}
				$config['new_image']=$config['upload_path'].$new_file_name;
				$this->load->library('image_lib',$config);
				if ( ! $this->image_lib->resize())
				{
				    echo $this->image_lib->display_errors();
				}else{
					$file_name=$new_file_name;
				}
			}
			if($this->input->get('preview')=='no'){
				$this->db->where(array('id'=>$userid));
				$this->db->update('users',array('avatar'=> $photo_base.$file_name));
			}
			$response['avatar']=$photo_base.$file_name;
			echo json_encode($response);
			return;
		}
	}
}
?>