<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=460, initial-scale=1.0"/>
	<title><?php echo html_purify($info->name); ?></title>
	<base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="e/css/main.css">
	<link rel="stylesheet" type="text/css" href="e/css/common.css">
</head>
<body>
<div class="container">
	<?php 
	$this->view('navbar');
	?>
	<div class="row section">
		<div class="row col s12">
			<div class="col s12" style="text-align: center;">
				<img class="avatar" style="width: 160px;height: 160px;padding: 0" src="<?php echo $info->avatar; ?>">
			</div>
		</div>
		<div class="col s12" style="text-align: center;">
			<div style="font-weight: bold;">
				<?php echo html_purify($info->name); 
				if(!empty($info->nickname)){
					echo " (".html_purify($info->nickname).")";
				}
				?>
			</div>
			<div>
				<?php echo html_purify($info->phone); ?>
			</div>
			<div>
				<?php echo html_purify($info->username.$info->college);
				?>
			</div>
		</div>
	</div>
	<div class="divider"></div>
	<div class="row section">
		<div class="col s6">
			<div class="full-info-container" style="background: #fff;border: none;box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14); padding: 12px;width: 90%;border-radius: 0;color: #333;">
				<?php
				if(empty($info->about))echo "<!--";
				?>
				<div style="padding: 4px 16px 16px;text-align: center;">
					<span><?php echo html_purify($info->about); ?></span>
				</div>
				<div class="divider" style="margin-bottom: 8px;"></div>
				<?php
					if(empty($info->about))echo "-->";
				?>
				<?php
					if(empty($info->address))echo "<!--";
				?>
				<div class="info-div">
					<span class="bold">Address:</span>
					<span><?php echo html_purify($info->address); ?></span>
				</div>
				<?php
					if(empty($info->address))echo "-->";
				?>
				<?php
					if(empty($info->department))echo "<!--";
				?>
				<div class="info-div">
					<span class="bold">Department:</span>
					<span><?php echo html_purify($info->department); ?></span>
				</div>
				<?php
					if(empty($info->department))echo "-->";
				?>
				<?php
					if(empty($info->fondly_known_for))echo "<!--";
				?>
				<div class="info-div">
					<span class="bold">Fondly known for:</span>
					<span><?php echo html_purify($info->fondly_known_for); ?></span>
				</div>
				<?php
					if(empty($info->fondly_known_for))echo "-->";
				?>
				<?php
					if(empty($info->favourite_hangout_place_in_campus))echo "<!--";
				?>
				<div class="info-div">
					<span class="bold">Favourite hangout place in campus:</span>
					<span><?php echo html_purify($info->favourite_hangout_place_in_campus); ?></span>
				</div>
				<?php
					if(empty($info->favourite_hangout_place_in_campus))echo "-->";
				?>
				<?php
					if(empty($info->favourite_professor))echo "<!--";
				?>
				<div class="info-div">
					<span class="bold">Favourite professor:</span>
					<span><?php echo html_purify($info->favourite_professor); ?></span>
				</div>
				<?php
					if(empty($info->favourite_professor))echo "-->";
				?>
				<?php
					if(empty($info->most_memorable_moment_on_campus))echo "<!--";
				?>
				<div class="info-div">
					<span class="bold">Most memorable moment in campus:</span>
					<span><?php echo html_purify($info->most_memorable_moment_on_campus); ?></span>
				</div>
				<?php
					if(empty($info->most_memorable_moment_on_campus))echo "-->";
				?>
			</div>
			<script type="text/javascript">
				window.addEventListener('load',function(){
					if($('.full-info-container').children().length==0){
						var a=$('<div>');
						a.css({padding: '4px 16px','text-align': 'center'});
						a.text('Information not available');
						$('.full-info-container').html(a);
					}
				});
			</script>
		</div>
		<div class="col s6" style="position: relative;">
			<span class="char_count" style="position:absolute;top: -6px;right:15px;"></span>
			<div class="input-field" style="background: #fff;padding: 0px 12px;box-shadow: 0 1px 4px 0 rgba(0,0,0,0.14);margin-bottom: 15px;">
				<textarea name="comment" id="commentbox" class="materialize-textarea" style="border: none;outline: none;box-shadow: none;padding: 12px 0px;margin-bottom: 0;"><?php
						if($alreadyWritten){
							echo html_purify($comment['comment']);
						}
					?></textarea>
				<label for="commentbox" style="height: 24px;left: 12px;">Write about <?php echo html_purify(explode(' ', $info->name)[0]); ?></label>
	        </div>
	        <div class="data-holder" data-commentId="<?php
	        	if($alreadyWritten){
	        		echo $comment['id'];
	        	}else{
	        		echo "false";
	        	}
	        ?>"></div>
	        <button type="submit" class="add_update_comment_button <?php if($alreadyWritten){echo "update";}else{echo "submit";} ?>_comment waves-effect waves-light btn"><?php
	        if($alreadyWritten){
	        	echo "UPDATE";
	        }else{
	        	echo "POST";
	        }
	        ?></button>
	        <button class="delete_comment waves-effect waves-light btn" <?php if(!$alreadyWritten){echo "style='display:none;'";} ?>>Delete</button>
        </div>
	</div>
</div>
<script type="text/javascript" src="e/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="e/css/materialize/js/materialize.min.js"></script>
<script type="text/javascript">
	var targetId='<?php echo $info->id; ?>';
	var base_url='<?php echo base_url(); ?>';
</script>
<script type="text/javascript">
	var processing=false;
	function addComment(){
		var submit_comment_button=$('.submit_comment');
		var comment = $('#commentbox').val();
		
		if(processing==true){
			return;
		}

		if(comment==undefined||comment==''){
			Materialize.toast('Empty Testimonial!', 4000);
			return;
		}
		submit_comment_button.addClass('disabled');
		Materialize.toast('Posting...',500000,'loader');
		processing=true;
		$.ajax({
			url:base_url + 'home/addComment',
			method:'POST',
			data:{comment:comment,targetId:targetId},
			dataType:'json',
			success:function(r){
				processing=false;
				submit_comment_button.removeClass('disabled');
				$('#commentbox').val(r.comment);
				$('.submit_comment').html('update').addClass('update_comment').removeClass('submit_comment');
				$('.delete_comment').css({display:''});
				$('.data-holder').attr('data-commentid',r.commentId);
				Materialize.toast('Testimonial posted', 4000);
				onKeyUp();
			},
			error:function(){
				processing=false;
				submit_comment_button.removeClass('disabled');
				Materialize.toast('Server not reachable',5000);
			}
		});
	}
	function updateComment(){
		var update_comment_button=$('.update_comment');
		var commentId = $('.data-holder').attr('data-commentId');
		var comment = $('#commentbox').val();

		if(processing==true){
			return;
		}
		if(comment==undefined||comment==''){
			Materialize.toast('Empty Testimonial!', 4000);
			return;
		}
		update_comment_button.addClass('disabled');
		processing=true;
		Materialize.toast('Updating...',500000,'loader');
		$.ajax({
			url:base_url + 'home/updateComment',
			method:'POST',
			data:{id:commentId,comment:comment},
			dataType:'json',
			success:function(r){
				processing=false;
				update_comment_button.removeClass('disabled');
				Materialize.toast('Testimonial updated', 4000);
				onKeyUp();
			},
			error:function(){
				processing=false;
				update_comment_button.removeClass('disabled');
				Materialize.toast('Server not reachable',500000);
			}
		})
	}
	function handle(){
		if($('.add_update_comment_button').hasClass('update_comment')){
			updateComment();
		}else{
			addComment();
		}
	}
	$('.add_update_comment_button').on('click',handle);
	$('.delete_comment').on('click',function(){
		var delete_comment_button=$('.delete_comment');
		var commentId = $('.data-holder').attr('data-commentId');
		if(processing==true){
			return;
		}
		delete_comment_button.addClass('disabled');
		processing=true;
		Materialize.toast('Deleting...',500000,'loader');
		$.ajax({
			url:base_url + 'home/deleteComment',
			method:'POST',
			data:{id:commentId,targetId:targetId},
			dataType:'json',
			success:function(r){
				processing=false;
				delete_comment_button.removeClass('disabled');
				if(r.success){
					$('#commentbox').val('');
					$('.delete_comment').css({display:'none'});
					$('.update_comment').html('post').addClass('submit_comment').removeClass('update_comment');
					Materialize.toast('Testimonial Deleted', 4000);
				}else{
					Materialize.toast('Some error occurred',500000);
				}
				onKeyUp();
			},
			error:function(){
				processing=false;
				delete_comment_button.removeClass('disabled');
				Materialize.toast('Server not reachable',500000);
			}
		})
	});
</script>
<script type="text/javascript" src="e/js/os/main.js"></script>
<script type="text/javascript">
	function onKeyUp(){
		var length=$('textarea').val().length;
		if(length==0){
			$('.char_count').text('');
		}else{
			if(length<1500){
				$('.char_count').text(length);
			}else{
				$('.char_count').text(length);
			}
		}
	}
	$('textarea').on('keyup',onKeyUp);
	window.addEventListener('load',function(){
		onKeyUp();
	});
</script>
</body>
</html>