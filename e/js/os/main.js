if(window.Materialize==undefined){
	window.Materialize={};
}

window.Materialize.toast=function(text,time,type){
	if(type==undefined){

	}
	var loader=null;
	if(type=='loader'){
		loader=(function(text){
			var l=$('<div>');
			l.addClass('loader');
			var spinner=$('<span>').addClass('loadingSpinner');
			l.append(spinner);
			var a=$('<span>').html(text);
			l.append(a);
			console.log(l)
			return l;
		})(text);
	}
	var toast=$('#bToast');
	if(toast[0]==undefined){
		var t=$('<div>').attr('id','bToast');
		$('body').append(t);
		toast=t;
	}
	var innerToast=$('<div>');
	innerToast.html(text).addClass('b-toast go-down');
	if(type=='loader'){
		innerToast.html(loader);
		console.log(innerToast)
	}
	toast.html('').append(innerToast);
	setTimeout(function(){
		innerToast.removeClass('go-down');
		setTimeout(function(){
			innerToast.addClass('go-down');
			setTimeout(function(){innerToast.remove();},400);
		},time);
	},100);
}
window.Materialize.clearToast=function(){
	$('.b-toast').addClass('go-down');
}