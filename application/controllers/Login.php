<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function index()
	{
		$this->load->view('login');
	}
	public function auth(){
		$this->load->helper('login');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$async=$this->input->post('async');
		if(!empty($async)){
			$async=TRUE;
		}else{
			$async=FALSE;
		}
		$r=[];
		$r['success']=FALSE;
		if(login($username,$password)){
			if(!$async){
				redirect('/','refresh');
			}
			$r['success']=TRUE;
			echo json_encode($r);
		}else{
			$r['reason']="Username and password doesn't match";
			echo json_encode($r);
			if(!$async){
				redirect('/login','refresh');
			}
		}
	}
	public function signup(){
		$this->load->view('signup');
	}
	public function confirm_email(){
		$this->load->helper('login');
		$id=$this->input->get('i');
		$username=$this->input->get('e');
		$confirm_token=$this->input->get('t');
		if(confirm($username,$id,$confirm_token)){
			$this->load->view('confirm');
		}else{
			echo "Invalid token!";
		}
	}
	public function register(){
		$username='amitkum';
		$college="@iitk.ac.in";
		$password='saharan';
		$async=$this->input->post('async');
		if(!empty($async)){
			$async=TRUE;
		}else{
			$async=FALSE;
		}
		$r=[];
		$r['success']=FALSE;

		$name=$this->input->post('name');
		$username=$this->input->post('username');
		$password=$this->input->post('password');
		$hostel=$this->input->post('hostel');
		$department=$this->input->post('department');
		$phone=$this->input->post('phone');
		$roll=$this->input->post('roll');
		if(empty($name)||empty($username)||empty($password)){
			$r['reason']='Name, username and password are required fields';
			if(!$async){redirect('/login/signup','refresh');}
			echo json_encode($r);
			return;
		}
		$gender='M';
		$this->load->helper('login');
		$signupstatus=signup($username,$name,$password,$gender,$college,$hostel,$department,$roll,$phone);
		if($signupstatus){
			$msg= "An email has been sent to ".$username.$college.". Please verify your email. It might take few minutes.";
			$r['msg']=$msg;
			$r['success']=TRUE;
			echo json_encode($r);
		}else{
			$r['reason']='This email has already been registered';
			echo json_encode($r);
			if(!$async){
				redirect('/login/signup','refresh');
			}
		}
	}
	public function logout(){
		$this->load->helper('login');
		logout();
		redirect('/','refresh');
	}
	public function forgot_password(){
		$this->load->helper('login');
		$u=$this->input->get('u');
		$id=usernametoid($u);
		if($id==false){
			echo "Username does not exist.";
			return;
		}
		send_update_password_mail($id);
		echo "An email has been sent to ".$u.'@iitk.ac.in regarding forgot password.';
	}
	public function update_forgot_password(){
		$this->load->helper('login');
		$id=$this->input->get('i');
		$token=$this->input->get('t');
		$action_update=false;
		if($this->input->post('_action')=='update'){
			$id=$this->input->post('i');
			$token=$this->input->post('t');
			$new_pass=$this->input->post('p');
			$action_update=true;
		}
		$r=$this->db->get_where('users',array('id'=>$id))->result_array();
		if(sizeof($r)==0){
			return;
		}
		$r=$r[0];
		$p=get_last_chars_of_pass($r['password']);
		if($token==$p){
			if($action_update){
				updatepassword($id,'',$new_pass,true);
				echo "Password updated successfully.";
			}else{
				$this->load->view('update_password',array('id'=>$id,'token'=>$token));
			}
		}else{
			echo "fail";
		}
	}
}
