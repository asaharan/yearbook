<!DOCTYPE html>
<html>
<head>
	<title>Update Password</title>
</head>
<body>
	<form action="<?php echo base_url(); ?>login/update_forgot_password" method="POST">
		<input type="text" hidden value="update" name="_action">
		<input type="text" hidden value="<?php echo $id; ?>" name="i">
		<input type="text" hidden value="<?php echo $token; ?>" name="t">
		<input type="password" name="p">
		<button type="submit">Update</button>
	</form>
</body>
</html>