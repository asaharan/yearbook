<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function index()
	{
		$this->load->helper('login');
		if(isloggedin()){
			redirect('/profile','refresh');
		}else{
			
		}
		$this->load->view('welcome_message');
	}
	public function updatepreference(){
		$cid=$this->input->post('id');
		$preference=$this->input->post('p');
		$this->load->helper('login');
		$r=[];
		$r['success']=FALSE;
		$max_prefs=25;
		if(!isloggedin() || empty($cid)||is_nan($cid)||empty($preference)||is_nan($preference) || $preference < 1 || $preference > $max_prefs){
			json_encode($r);
			return;
		}
		$userid=userid();
		$this->db->where(array('targetId'=>$userid,'preference'=>$preference));
		$this->db->update('comments',array('preference'=>999));
		$af=$this->db->affected_rows();
		$this->db->where(array('targetId'=>$userid,'id'=>$cid));
		$this->db->update('comments',array('preference'=>$preference));
		$r['affected_rows']=$af;
		$r['success']=TRUE;
		echo json_encode($r);
	}
	public function addComment(){
		$r=[];
		$r['success']=false;
		$this->load->helper('login');
		$comment = $this->input->post('comment');
		$targetId = $this->input->post('targetId');
		if(empty($comment)){
			json_encode($r);
			$r['description']='Testimonial can not be empty';
			return;
		}
		if(!isloggedin()){
			echo json_encode($r);
			return;
		}
		$userid=userid();
		if($targetId==$userid){
			echo json_encode($r);
			return;
		}
		$x=$this->db->get_where('comments',array('targetId'=>$targetId,'ownerId'=>$userid,'visible'=>1),1)->result_array();
		if(sizeof($x)>0){
			$r['description']='You can only add one testimonial on your his timeline';
			echo json_encode($r);
			return;
		}
		$e=$this->db->insert('comments',array('targetId'=>$targetId,'comment'=>$comment,'ownerId'=>$userid));
		$commendId=$this->db->insert_id();
		$r['success']=true;
		$r['commentId']=$commendId;
		$r['comment']=$comment;
		echo json_encode($r);
	}
	public function chooseComment(){
		$id=$this->input->post('id');
		$response=[];
		$response['success']=false;
		if(empty($id)){
			echo json_encode($response);
			return;
		}
		$this->load->helper('login');
		if(isloggedin()){
			$userid=userid();
			$this->db->where(array('id'=>$id,'targetId'=>$userid));
			$this->db->update('comments',array('choosen'=>1));
			$n=$this->db->affected_rows();
			if($n==1){
				$response['success']=true;
			}
			echo json_encode($response);
		}else{
			echo $r;
		}
	}
	public function updateComment(){
		$id=$this->input->post('id');
		$comment = $this->input->post('comment');
		$response=[];
		$response['success']=false;
		if(empty($id)||empty($comment)){
			echo json_encode($response);
			return;
		}

		$this->load->helper('login');
		if(isloggedin()){
			$userid=userid();
			$this->db->where(array('id'=>$id,'ownerId'=>$userid));
			$this->db->update('comments',array('comment'=>$comment));
			if($this->db->affected_rows()>0){
				$response['success']=true;
			}else{
				$response['success']=false;
			}
			echo json_encode($response);
			return;
		}else{
			echo json_encode($response);
		}
	}
	public function removeFromChoosenComment(){
		$id=$this->input->post('id');
		$response=[];
		$response['success']=false;
		if(empty($id)){
			echo json_encode($response);
			return;
		}
		$this->load->helper('login');
		if(isloggedin()){
			$userid=userid();
			$this->db->where(array('id'=>$id,'targetId'=>$userid));
			$this->db->update('comments',array('choosen'=>0,'preference'=>999));
			$n=$this->db->affected_rows();
			if($n==1){
				$response['success']=true;
			}
			echo json_encode($response);
		}else{
			echo $r;
		}
	}
	public function deleteComment(){
		$id=$this->input->post('id');
		$response=[];
		$response['success']=false;
		if(empty($id)){
			echo json_encode($response);
			return;
		}
		$this->load->helper('login');
		if(isloggedin()){
			$userid=userid();
			$this->db->where(array('id'=>$id,'targetId'=>$userid));
			$this->db->update('comments',array('visible'=>0));
			$n=$this->db->affected_rows();

			$this->db->where(array('id'=>$id,'ownerId'=>$userid));
			$this->db->update('comments',array('visible'=>0));
			$m=$this->db->affected_rows();

			if($n==1||$m==1){
				$response['success']=true;
			}
			echo json_encode($response);
		}else{
			echo json_encode($response);
		}	
	}
	public function donate(){
		$this->load->helper('login');
		$r=[];
		$r['success']=FALSE;
		if(!isloggedin()){
			echo json_encode($r);
			$r['reason']="You are not logged in";
			return;
		}
		$amount=$this->input->post('amount');
		if(empty($amount)|| !is_numeric($amount) || $amount < -1){
			$r['reason']="Not a number";
			echo json_encode($r);
			return;
		}
		$userid=userid();
		$this->db->where(array('id'=>$userid));
		$this->db->update('users',array('donation_amount'=>$amount));
		$r['reason']="Thank you for your donation of Rs ".$amount;
		if($amount==-1){
			$r['reason']='Canceled your donation';
		}
		$r['success']=TRUE;
		echo json_encode($r);
	}
}
