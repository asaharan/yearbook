<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Y12 Yearbook IIT Kanpur</title>
	<base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<!-- <link rel="stylesheet" type="text/css" href="e/css/main.css"> -->
	<link rel="stylesheet" type="text/css" href="e/css/home.css">
	<link rel="stylesheet" type="text/css" href="e/css/common.css">
</head>
<body>
<div class="popup-material">
	<div class="popup-material-inner all-forms">
		<div class="popup-material-innerBox">
			<div class="logo">
				Yearbook
			</div>
			<div class="tagline">
				Memories are made to be remembered
			</div>
			<div class="form-container">
				<div class="form clearfix form-login">
					<div class="form-title">LOGIN</div>
					<div class="form-holder">
						<form id="loginForm" action="<?php echo base_url(); ?>login/auth" method="POST" autocomplete="off">
							<label>
								<legend>Username</legend>
								<input type="text" name="username" />
							</label>
							<label>
								<legend>Password</legend>
								<input type="password" name="password" />
							</label>
							<div>
								<button class="login">Login</button>
							</div>
							<div class="forgot-password-div" style="margin-top: 7px;display: none;">
								<a class="forgot-password-a" style="color: #3E78AD">Forgot password</a>
							</div>
						</form>
					</div>
				</div>
				<div class="form form-signup">
					<div class="form-title">SIGN UP</div>
					<div class="form-holder">
						<form id="signupForm" action="<?php echo base_url(); ?>login/register" method="POST" autocomplete="off">
							<label>
								<legend>Full name</legend>
								<input type="text" name="name" />
							</label>
							<label>
								<legend>IITK Username</legend>
								<input type="text" name="username" />
							</label>
							<label>
								<legend>Roll No.</legend>
								<input type="text" name="roll" />
							</label>
							<label class="untouched">
								<legend>Alternate Email</legend>
								<input type="text" value="" name="alternate_email" />
							</label>
							<label class="untouched">
								<legend>Phone No.</legend>
								<input type="text" value="" name="phone" />
							</label>
							<label class="untouched">
								<legend>Department</legend>
								<input type="text" value="" name="department" />
							</label>
							<label class="untouched">
								<legend>Hostel</legend>
								<input type="text" value="" name="hostel" />
							</label>
							<label>
								<legend>Password</legend>
								<input type="password" name="password" />
							</label>
							<div>
								<button class="login">Sign Up</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var base_url='<?php echo base_url(); ?>';
</script>
<script type="text/javascript" src="e/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript">
	var login=function(){
		var username=$('#loginForm input[name=username]').val();
		var password=$('#loginForm input[name=password]').val();
		if(username==''){
			Materialize.toast('Username should not be empty', 40000);
			return;
		}
		Materialize.toast('Working...',500000,'loader');
		$.ajax({
			url:base_url + 'login/auth',
			method:'POST',
			data:{username:username,password:password,async:true},
			dataType:'json',
			success:function(r){
				console.log('Yo');
				if(r.success){
					Materialize.toast('Login successful',500000);
					window.location.reload();
				}else{
					Materialize.toast(r.reason, 4000);
					$('.forgot-password-div').css({display:''});
					$('.forgot-password-a').attr('href',base_url+'login/forgot_password'+'?u='+username);
				}
			},
			error:function(r){
				console.log('error');
			}
		});
	};
	function isEmpty(e){
		return $.isEmptyObject(e);
	}
	function ucWords(a){
		a = a.toLowerCase().replace(/\b[a-z]/g, function(l) {
		    return l.toUpperCase();
		});
		return a;
	}
	var signup=function(){
		var e={};
		e.name=$('#signupForm input[name=name]').val();
		e.username=$('#signupForm input[name=username]').val();
		e.roll=$('#signupForm input[name=roll]').val();
		e.alternate_email=$('#signupForm input[name=alternate_email]').val();
		e.phone=$('#signupForm input[name=phone]').val();
		e.password=$('#signupForm input[name=password]').val();
		e.hostel=$('#signupForm input[name=hostel]').val();
		e.department=$('#signupForm input[name=department]').val();
		var optionals={department:true,hostel:true,phone:true,alternate_email:true};
		for( a in e){
			if(optionals[a]==true){continue;}
			if(isEmpty(e[a])){
				a=a.replace('_',' ');
				Materialize.toast( ucWords(a)+" should not be empty",4000);
				return;
			}else{
				if(a=='username'){
					e[a]=e[a].split('@')[0];
				}
			}
		}
		e.async=true;
		$.ajax({
			url:base_url + 'login/register',
			method:'POST',
			data:e,
			dataType:'json',
			success:function(r){
				if(r.success){
					Materialize.toast(r.msg, 40000);
					$('#signupForm input').val('');
					$('.untouched').removeClass('visible');
				}else{
					Materialize.toast(r.reason, 4000);
				}
			}
		})
	}
	window.addEventListener('load',function(){
		$('#loginForm').on('submit',function(e){
			e.preventDefault();
			login();
			return false;
		});
		$('#signupForm').on('submit',function(e){
			e.preventDefault();
			signup();
		});
	});
</script>
<!-- <script type="text/javascript" src="e/css/materialize/js/materialize.min.js"></script> -->
<script type="text/javascript" src="e/js/os/main.js"></script>