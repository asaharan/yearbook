<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=460, initial-scale=1.0"/>
	<title><?php echo html_purify($info->name); ?></title>
	<base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="e/css/main.css">
	<link rel="stylesheet" type="text/css" href="e/css/common.css">
	<link rel="stylesheet" type="text/css" href="e/css/cropper.min.css">
	<?php
		function preferencer($cid,$preference,$max_prefs,$preferenceChoosen){
			$r="<span class='pref-container clearfix'>";
			$r.="<span style='float:left;padding: 1px 22px;border-right:solid 1px #fafafa;background:#f0f0f0;'>Preference </span>";
			$class='pref-btn';
			for ($i=1; $i <= $max_prefs; $i++) {
				$class='pref-btn';
				if($i==$preference){
					$class.=' active';
				}
				if($preferenceChoosen[$i]===TRUE){
					$class.=' already';
				}
				$r .= "<span class='$class' data-cid='$cid' data-currentPreference='$preference' data-choosePreference='$i'>$i</span> ";
			}
			$r.="</span>";
			return $r;
		}
		function renderComment($i,$comments,$removeOrChoose){
			if(empty($removeOrChoose)){
				$removeOrChoose='Choose';
			}
			$max_prefs=25;
			$preferenceChoosen=array('1'=>FALSE,'2'=>FALSE,'3'=>FALSE,'4'=>FALSE,'5'=>FALSE,'6'=>FALSE,'7'=>FALSE);
			for($m=1;$m <= $max_prefs;$m++){
				$preferenceChoosen[$m]=FALSE;
			}
			for($j=0;$j<sizeof($comments);$j++){
				$c=$comments[$j];
				if($c['preference']>=1&&$c['preference']<=$max_prefs){
					$preferenceChoosen[$c['preference']]=TRUE;
				}
			}
			for(;$i<sizeof($comments);$i+=2){
				$comment=$comments[$i];
				echo "<div class='comment'>";
					echo "<div class='profile-link'>";
						echo "<a href='".base_url().'profile/'.html_purify($comment['username'])."' class='clearfix float-left'>";
						echo "<img class='avatar small-avatar profile-image' src='".$comment['avatar']."'/>";
						echo "</a>";
						echo "<a href='".base_url().'profile/'.html_purify($comment['username'])."' class='profile-name comment-name'>".html_purify($comment['name']).'</a>';
						echo "<span class='gray-color'>wrote on your timeline</span>"."<span";
						if((strlen($comment['comment'])>1000)){
							echo " title='This testimonial is too large'";
						}
						echo "> (".strlen($comment['comment']).")</span>";
					echo "</div>";
					echo "<div class='comment-text'>".$comment['comment']."</div>";
					echo "<div class='comment-options'>";
						echo "<span data-id='".$comment['id']."' class='btn waves-effect waves-light comment-".$removeOrChoose."'> ".$removeOrChoose."</span>";
						if($removeOrChoose=='Remove'){
							echo preferencer($comment['id'],$comment['preference'],$max_prefs,$preferenceChoosen);
						}
						// echo "<span data-id='".$comment['id']."' class='btn waves-effect waves-light comment-Delete'>Delete</span>";
					echo "</div>";
				echo "</div>";
			}
		}
	?>
	<style type="text/css">
		.pref-container{
			line-height: 36px;
			display: inline-block;
			box-shadow: 0 0px 3px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
			margin: 10px auto 0;
			transition: all ease 400ms;
		}
		.pref-container:hover{
			box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
		}
		.pref-btn{
			float: left;
			height: 39px;
			width: 39px;
			display: inline-block;
			text-align: center;
			border-right: solid 1px #fafafa;
			border-bottom: solid 1px #fafafa;
			cursor: pointer;
			transition: background-color ease 400ms;
		}
		.pref-btn.already{
			background: #f0f0f0;
		}
		.pref-btn.active {
		    background: #2196F3;
		    color: #fff;
		}
		.comment-Remove{
			float: left;
		}
	</style>
</head>
<body>
<div class="container">
	<?php 
	$this->view('navbar');
	?>
	<div class="msg profile-pic" style="display:none;"></div>
	<a class="donate" href="<?php echo base_url(); ?>donate" style="display: <?php if ($info->donation_amount>0) {
		echo "none";
	}else{echo "block";} ?>;background: #fff;color: #3F51B5;padding: 11px 19px;font-size: 16px;margin: 7px auto;border-radius: 3px;max-width: 600px;text-align:center;">
		Donate for <b>Senior Classroom Gift</b>
	</a>
	<div style="text-align: center;margin: 5px 0;">
	<a href="bestProfessor" class="aboutButton" style="text-transform: uppercase;color: #fff;padding: 12px 24px;background: #2196F3;border-radius: 3px;display: inline-block;cursor: pointer;box-shadow: 0 2px 3px 0 rgba(0,0,0,0.2);">Vote best professor award</a>
	</div>
	<div class="row section">
		<div class="row col s12">
			<div class="col s12" style="text-align: center;position: relative;">
				<div class="avatar-container" style="position: relative;width: 260px;height: 160px;display: inline-block;">
					<form id="dp-uploader" class="dp-uploader" action="<?php echo base_url(); ?>upload/dp/do_upload" enctype="multipart/form-data" method="post" accept-charset="utf-8">
						<span style="background: no-repeat url(e/img/icon/cam-trash.png) 0 0;display: inline-block;height: 20px;margin: 9px 8px;width: 24px;"></span>
						<input class="dp-uploader-input" type="file" accept="image/*" name="userfile" size="20" />
					</form>
					<img id="avatar" class="avatar" style="width: 160px;height: 160px;padding: 0" src="<?php echo $info->avatar; ?>">
					<div class="profile-update-options" style="display: none;">
						<a class="crop-and-save" onclick="uploadCroppedDP();">Crop and Save</a>
						<a class="cancel-crop">Cancel</a>
					</div>
				</div>
			</div>
		</div>
		<div class="col s12" style="text-align: center;">
			<div style="font-weight: bold;">
				<?php echo html_purify($info->name); 
				if(!empty($info->nickname)){
					echo " (".html_purify($info->nickname).")";
				}
				?>
			</div>
			<div>
				<?php echo html_purify($info->phone); ?>
			</div>
			<div>
				<?php echo html_purify($info->username.$info->college);
				?>
			</div>
		</div>
	</div>
	<div class="divider"></div>
	<div class="row">
		<div class="col s12">
			<div style="font-weight: bold;padding: 10px;"><?php if(sizeof($choosenComments)==0){echo "No c";}else{echo "C";} ?>hoosen testimonials for your yearbook</div>
		</div>
		<div class="col s6">
			<?php
				renderComment(0,$choosenComments,'Remove');
			?>
		</div>
		<div class="col s6">
			<?php 
				renderComment(1,$choosenComments,'Remove');
			?>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div style="font-weight: bold;padding: 10px;"><?php if(sizeof($pendingComments)==0){echo "No ";} ?>Pending Testimonials</div>
		</div>
		<div class="col s6">
			<?php
				renderComment(0,$pendingComments,'Choose');
			?>
		</div>
		<div class="col s6">
			<?php 
				renderComment(1,$pendingComments,'Choose');
			?>
		</div>
	</div>
</div>
<script type="text/javascript" src="e/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="e/js/jquery.form.js"></script>
<script type="text/javascript" src="e/js/cropper.min.js"></script>
<script type="text/javascript" src="e/css/materialize/js/materialize.min.js"></script>
<script type="text/javascript">
	var targetId='<?php echo $info->id; ?>';
	var base_url='<?php echo base_url(); ?>';
	var imageUploaded=<?php echo $info->avatar=='e/img/default-user.jpg'?'false':'true'; ?>
</script>
<script type="text/javascript">
	!function(t){"use strict";var e=t.HTMLCanvasElement&&t.HTMLCanvasElement.prototype,o=t.Blob&&function(){try{return Boolean(new Blob)}catch(t){return!1}}(),n=o&&t.Uint8Array&&function(){try{return 100===new Blob([new Uint8Array(100)]).size}catch(t){return!1}}(),r=t.BlobBuilder||t.WebKitBlobBuilder||t.MozBlobBuilder||t.MSBlobBuilder,a=/^data:((.*?)(;charset=.*?)?)(;base64)?,/,i=(o||r)&&t.atob&&t.ArrayBuffer&&t.Uint8Array&&function(t){var e,i,l,u,b,c,d,B,f;if(e=t.match(a),!e)throw new Error("invalid data URI");for(i=e[2]?e[1]:"text/plain"+(e[3]||";charset=US-ASCII"),l=!!e[4],u=t.slice(e[0].length),b=l?atob(u):decodeURIComponent(u),c=new ArrayBuffer(b.length),d=new Uint8Array(c),B=0;B<b.length;B+=1)d[B]=b.charCodeAt(B);return o?new Blob([n?d:c],{type:i}):(f=new r,f.append(c),f.getBlob(i))};t.HTMLCanvasElement&&!e.toBlob&&(e.mozGetAsFile?e.toBlob=function(t,o,n){t(n&&e.toDataURL&&i?i(this.toDataURL(o,n)):this.mozGetAsFile("blob",o))}:e.toDataURL&&i&&(e.toBlob=function(t,e,o){t(i(this.toDataURL(e,o)))})),"function"==typeof define&&define.amd?define(function(){return i}):"object"==typeof module&&module.exports?module.exports=i:t.dataURLtoBlob=i}(window);
</script>
<script type="text/javascript">
	$('.comment-Choose').on('click',function(){
		var commentId=$(this).attr('data-id');
		$.ajax({
			method:'POST',
			url:base_url + 'home/chooseComment',
			data:{id:commentId},
			dataType:'json',
			success:function(r){
				console.log(r)
				if(r.success){
					window.location.reload();
				}
			}
		});
	});
	$('.comment-Remove').on('click',function(){
		var commentId=$(this).attr('data-id');
		$.ajax({
			method:'POST',
			url:base_url + 'home/removeFromChoosenComment',
			data:{id:commentId},
			dataType:'json',
			success:function(r){
				console.log(r)
				if(r.success){
					window.location.reload();
				}
			}
		});
	});
	$('.comment-Delete').on('click',function(){
		var commentId=$(this).attr('data-id');
		$.ajax({
			method:'POST',
			url:base_url + 'home/deleteComment',
			data:{id:commentId},
			dataType:'json',
			success:function(r){
				console.log(r)
				if(r.success){
					window.location.reload();
				}
			}
		});
	});
	$('.cancel-crop').on('click',function(){
		cropper.destroy();
		$('.avatar-container .avatar').attr('src',oldAvatar);
		$('.profile-update-options').css({display:'none'});
	})
	var cropper=null;
	function editDP(){
		var image = document.getElementById('avatar');
		cropper = new Cropper(image, {
		  aspectRatio: 16 / 16,
		  modal:false,
		  dragMode:'move',
		  autoCrop:true,
		  cropBoxMovable:false,
		  cropBoxResizable:false,
		  minCropBoxWidth:160,
		  viewMode:3,
		  guides:false,
		  center:false,
		  highlight:true,
		  crop: function(e) {
		  }
		});

	}
	var dpUploader=$('.dp-uploader');	
	var dpUploadUrl=dpUploader.attr('action');
	var oldAvatar='';
	function dpUpdated(r,justEdited){
		if(r.success){
			Materialize.toast('Successfuly updated profile pic',3000);
			$('.avatar-container .avatar').attr('src',r.avatar);
			$('.avatar-container').css({opacity:''});
			if(justEdited){
				$('.profile-update-options').css({display:'none'});
				cropper.destroy();
			}else{
				editDP();
			}
		}else{
			Materialize.toast(r.error,3000);
		}
	}
	function uploadCroppedDP(){
		console.log(cropper.getCroppedCanvas())
		var dataurl=cropper.getCroppedCanvas().toDataURL('image/png');
		Materialize.toast('Saving...',3000000,'loader');
		cropper.getCroppedCanvas().toBlob(function (blob) {
			var formData = new FormData();
			formData.append('userfile', blob);
			$.ajax(dpUploadUrl+'?preview=no', {
			    method: "POST",
			    data: formData,
			    processData: false,
			    contentType: false,
			    dataType:'json',
			    success: function (r) {
			    	dpUpdated(r,true);	
			    },
			    error: function () {
			    	Materialize.toast('Network problem.',3000000);	
			    }
			});
		});
	}
	function uploadDP(){
		var dpUploader=$('.dp-uploader');	
		var url=dpUploader.attr('action');
		Materialize.toast('Uploading image...',3000000,'loader');
		$('#dp-uploader').ajaxSubmit({
			url:url,
			type:'post',
			dataType:'json',
			success:function(r){
				if(r.success){
					Materialize.toast('Working...',3000000,'loader');
					oldAvatar=$('.avatar-container .avatar').attr('src');
					$('.avatar-container').css({opacity:1});
					$('.avatar-container .avatar').attr('src',r.avatar).on('load',function(){
						Materialize.clearToast();
					});
					editDP();
					$('.profile-update-options').css({display:''});
				}else{
					Materialize.toast(r.error,3000);
				}
			}
		});
		return;
	}
	$('.dp-uploader-input').on('change',function(){
		var file = this.files[0];
		console.log(file);
		if(file==undefined){
			return;
		}
		var filesize=file.size;
		var maxfilesize=10*1024*1024;
		if(filesize>maxfilesize){
			Materialize.toast('File size should be less than 10Mb ',4000);
			return;
		}
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			Materialize.toast('Please Select A valid Image File',4000);
			return false;
		}
		else
		{
			uploadDP();
		}
	});
	if(!imageUploaded){
		$('.msg.profile-pic').css('display','').text('Please upload your profile picture. Your profile picture will be used in yearbook.');
	}else{
		$('.msg.profile-pic').css('display','none');
	}

	$('.pref-btn').on('click',function(e){
		var clickedButton=$(e.target);
		var currP=clickedButton.attr('data-currentpreference');
		var chooseP=clickedButton.attr('data-choosepreference');
		var cid = clickedButton.attr('data-cid');
		Materialize.toast('Working...',30000000,'loader');
		$.ajax({
			url:base_url + 'home/updatepreference',
			method:'POST',
			data:{id:cid,p:chooseP},
			dataType:'json',
			success:function(r){
				var affected_comments=null;
				if(r.success){
					affected_comments = $('[data-currentpreference='+chooseP+']');
					console.log(affected_comments);
					$('[data-cid='+cid+']').removeClass('active').attr('data-currentpreference',chooseP);
					$('.pref-btn[data-choosepreference='+currP+']').removeClass('already');
					$('[data-choosepreference='+chooseP+']').addClass('already');
					affected_comments.removeClass('active');
					clickedButton.addClass('active');
					Materialize.toast('Preferences updated.',3000);
				}else{
					Materialize.toast('Some error occurred.',3000);
				}
			},
			error:function(){
				Materialize.toast('Some error occurred.',3000);
			}
		});
	})
</script>
<script type="text/javascript" src="e/js/os/main.js"></script>
</body>
</html>