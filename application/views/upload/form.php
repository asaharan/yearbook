<html>
<head>
<title>Submit your abstract for IMP | Techkriti IIT Kanpur</title>
<style type="text/css">

	.wrapper{
		font-family: sans-serif;
		text-align: center;
		padding-top: 7%;
	}
	form{
		margin-bottom: 0;
	}
	.shadow{
		display: inline-block;
		width:300px;
		padding: 20px;
		text-align: center;
		box-shadow:0 1px 3px 0 rgba(0,0,0,.2),0 1px 1px 0 rgba(0,0,0,.14),0 2px 1px -1px rgba(0,0,0,.12);
	}
	[type=submit]{
		color: #fff;
		background-color: rgb(77, 167, 201);
		border:none;
		padding: 9px 50px;
		border-radius: 100px;
		font-weight: bold;
	}
</style>
</head>
<body>
<div class="wrapper">
	<img src="techkriti-logo.png" style="width:200px;">
	<div style="display:<?php if(empty($_COOKIE['skill_user'])){echo "block";}else{echo "none";} ?>;margin-bottom:20px;">
		<span>You need to login to submit your abstract.</span>
		<br>
		<a href="http://portal.techkriti.org/#/?next=http://2016.techkriti.org/INAE/">Click here</a> to login.
	</div>
	<?php if(empty($_COOKIE['skill_user'])){die();}?>
	<h4>Submit you abstract for <a href="../IMP">IMP</a></h4>
	<div class="shadow">
		<?php echo $error;?>
		<form action="<?php echo base_url(); ?>upload/dp/do_upload" enctype="multipart/form-data" method="post" accept-charset="utf-8">
		<input type="file" accept="image/*" name="userfile" size="20" />

		<br /><br />

		<input type="submit" value="Submit" />

		</form>
	</div>
	<div style="color:#C51007;font-size:14px;">
		<?php 
		if(!empty($_GET['msg'])){
			$error=$_GET['msg'];
			echo $error;
		}
		?>
	</div>
</div>