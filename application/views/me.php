<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=460px, initial-scale=1.0"/>
	<title><?php echo $info['name']?></title>
	<base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="e/css/main.css">
</head>
<body>
<div class="container">
	<?php $this->view('navbar');?>
	<div style="display: <?php if($msg==''){echo "none";}else{echo "''";} ?>" class="msg"><?php echo $msg; ?></div>
	<div class="form-title">Your profile</div>
	<form action="<?php echo base_url(); ?>me/update" autocomplete="off" method="post" class="bform">
		<label>
			<legend>Name</legend>
			<input type="text" name="user[name]" value="<?php echo $info['name']?>" />
		</label>
		<label>
			<legend>About me</legend>
			<input type="text" name="user[about]" value="<?php echo $info['about']?>" />
		</label>
		<label>
			<legend>Nickname</legend>
			<input type="text" name="user[nickname]" value="<?php echo $info['nickname']?>" />
		</label>
		<label>
			<legend>Roll no.</legend>
			<input type="text" name="user[roll]" value="<?php echo $info['roll']?>" />
		</label>
		<label>
			<legend>Alternate email</legend>
			<input type="text" name="user[alternate_email]" value="<?php echo $info['alternate_email']?>" />
		</label>
		<label>
			<legend>Department</legend>
			<input type="text" name="user[department]" value="<?php echo $info['department']?>" />
		</label>
		<label>
			<legend>Address</legend>
			<input type="text" name="user[address]" value="<?php echo $info['address']?>" />
		</label>
		<label>
			<legend>Fondly known for</legend>
			<input type="text" name="user[fondly_known_for]" value="<?php echo $info['fondly_known_for']?>" />
		</label>
		<label>
			<legend>Favourite hangout place in campus</legend>
			<input type="text" name="user[favourite_hangout_place_in_campus]" value="<?php echo $info['favourite_hangout_place_in_campus']?>" />
		</label>
		<label>
			<legend>Favourite Professor</legend>
			<input type="text" name="user[favourite_professor]" value="<?php echo $info['favourite_professor']?>" />
		</label>
		<div class="input-field">
			<textarea name="user[most_memorable_moment_on_campus]" id="most_memorable_moment_on_campus" class="materialize-textarea" style="height: 22px;min-height: 40px;padding: 0 8px;"><?php echo $info['most_memorable_moment_on_campus']; ?></textarea>
			<label for="most_memorable_moment_on_campus" style="height: 24px;">Most memorable moment on campus</label>
		</div>
		<div>
			<input name="action" value="update" hidden />
		</div>
		<div>
			<button type="submit" class="waves-effect waves-light btn">Update profile</button>
		</div>
	</form>
	<script type="text/javascript">
		var base_url='<?php echo base_url(); ?>';
	</script>
<script type="text/javascript" src="e/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="e/css/materialize/js/materialize.min.js"></script>
<script type="text/javascript" src="e/js/os/main.js"></script>
<script type="text/javascript">

</script>