<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Me extends CI_Controller {

	public $possibleUpdate=
		array(
			'name'=>TRUE,
			'username'=>FALSE,
			'college'=>FALSE,
			'confirm'=>FALSE,
			'confirm_token'=>FALSE,
			'password'=>FALSE,
			'nickname'=>TRUE,
			'roll'=>TRUE,
			'alternate_email'=>TRUE,
			'department'=>TRUE,
			'address'=>TRUE,
			'fondly_known_for'=>TRUE,
			'avatar'=>FALSE,
			'gender'=>TRUE,
			'hostel'=>TRUE,
			'about'=>TRUE,
			'favourite_hangout_place_in_campus'=>TRUE,
			'most_memorable_moment_on_campus'=>TRUE,
			'favourite_professor'=>TRUE,
			'action'=>FALSE,
			'about'=>TRUE
		);

	public function index()
	{
		$this->load->helper('login');
		$msg=$this->input->get('s');
		if($msg=='s'){
			$msg='Profile updated successfully';
		}
		if(isloggedin()){
			$userid=userid();
			$c=0;
			$select='';
			foreach ($this->possibleUpdate as $key => $value) {
				if($value!=FALSE){
					if($c!=0){
						$select.=',';
					}
					$c++;
					$select.=$key;
				}
			}
			// echo $select;
			$this->db->select($select);
			$info=$this->db->get_where('users',array('id'=>$userid))->result_array()[0];
			// echo json_encode($info);
			$this->load->view('me',array('info'=>$info,'msg'=>$msg));
		}else{
			redirect('/','refresh');
		}
	}
	public function update(){
		$this->load->helper('login');
		if(!isloggedin()){
			redirect('/');
			return;
		}
		$userid=userid();
		
		$x=$this->input->post('user');
		// echo json_encode($x);
		$updates=array();
		foreach ($x as $key => $value) {
			$key=strtolower($key);
			if($this->possibleUpdate[$key]!=FALSE){
				$updates[$key]=$value;
			}
		}
		$this->db->where(array('id'=>$userid));
		$this->db->update('users',$updates);
		redirect('/me?s=s');
	}
}