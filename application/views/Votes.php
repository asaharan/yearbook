<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="user-scalable=no,initial-scale=1,maximum-scale=1">
	<base href="<?php echo base_url(); ?>"></base>
	<title>Best Professor Award</title>
	<link rel="stylesheet" type="text/css" href="e/js/vote/public/css/main.css">
</head>
<body>
<div id="ReactROOT"></div>
<script type="text/javascript" src="e/js/vote/public/js/bundle.js"></script>
</body>
</html>