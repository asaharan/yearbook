<!DOCTYPE html>
<html>
<head>
	<title>Yearbook</title>
	<!-- <link rel="stylesheet" type="text/css" href="e/css/yearbook.css"> -->
	<style type="text/css">
		body{margin:0;background:#fafafa}*,*:after,*:before{box-sizing:border-box}.clearfix:after,.person-data-container .profile-container:after,.person-data-container .profile-container .avatar-container:after,.image-owner-target-comment-container .image-owner-target-container:after{visibility:hidden;display:block;font-size:0;content:" ";clear:both;height:0}.clearfix,.person-data-container .profile-container,.person-data-container .profile-container .avatar-container,.image-owner-target-comment-container .image-owner-target-container{display:inline-block}.float-left{float:left}.loadingSpinner{border:2px solid #4285f4;border-top-color:transparent;border-radius:50%;-webkit-animation:spin 1.1s infinite linear;animation:spin 1.1s infinite linear;border-color:white;border-top-color:transparent;display:inline-block;height:16px;margin-right:12px;vertical-align:middle;width:16px}.float-right{float:right}.gray-color{color:grey}.bold{font-weight:bold}@-webkit-keyframes spin{from{-webkit-transform:rotate(0)}to{-webkit-transform:rotate(360deg)}}@-moz-keyframes spin{from{-moz-transform:rotate(0)}to{-moz-transform:rotate(360deg)}}@keyframes spin{from{-webkit-transform:rotate(0);-moz-transform:rotate(0);-ms-transform:rotate(0);-o-transform:rotate(0);transform:rotate(0)}to{-webkit-transform:rotate(360deg);-moz-transform:rotate(360deg);-ms-transform:rotate(360deg);-o-transform:rotate(360deg);transform:rotate(360deg)}}body{background-color:#f0f0f0}[data_force_hide=true]{display:none !important}a{color:inherit;text-decoration:none}.index{margin:12px 0}.index .name-first-char{display:block;width:100%;font-weight:bold;text-transform:uppercase;font-size:30px;margin-top:12px}.index .profile-link-container{width:32%;display:inline-block}.index a{color:#2196f3;text-decoration:none}.index a:hover{text-decoration:underline}.capitalize{text-transform:capitalize}.all-comment-container{position:relative;width:924px;display:block;margin:auto;padding:12px}.person-data-container{position:relative;padding:12px 0;border-top:solid 1px #aaa}.person-data-container .profile-container{background:#fff;padding:12px;border-radius:4px;width:100%;display:-webkit-box;display:-moz-box;display:box;display:-webkit-flex;display:-moz-flex;display:-ms-flexbox;display:flex;-webkit-box-orient:horizontal;-moz-box-orient:horizontal;box-orient:horizontal;-webkit-box-direction:normal;-moz-box-direction:normal;box-direction:normal;-webkit-flex-direction:row;-moz-flex-direction:row;flex-direction:row;-ms-flex-direction:row;-webkit-box-pack:start;-moz-box-pack:start;box-pack:start;-webkit-justify-content:flex-start;-moz-justify-content:flex-start;-ms-justify-content:flex-start;-o-justify-content:flex-start;justify-content:flex-start;-ms-flex-pack:start}.person-data-container .profile-container .avatar-container{width:120px;display:inline-block;margin-right:12px}.person-data-container .profile-container .avatar-container .avatar{height:120px;width:120px;float:left;border-radius:4px}.person-data-container .profile-container .profile-detail-container .colorfull{color:#346894}.person-data-container .profile-container .profile-detail-container .name{font-weight:bold}.person-data-container .profile-container .profile-detail-container .aboutme{margin-top:12px}.image-owner-target-comment-container{background-color:#fff;padding:12px;margin:12px 0;border-radius:4px;color:#333}.image-owner-target-comment-container .image-owner-target-container{position:relative;line-height:40px}.image-owner-target-comment-container .image-owner-target-container .image-container{float:left;margin-right:12px}.image-owner-target-comment-container .image-owner-target-container .image-container .owner-image{float:left;position:relative;display:inline-block;width:40px;height:40px;border-radius:50%;border:none}.image-owner-target-comment-container .image-owner-target-container .owner-name{float:left;display:inline-block}.image-owner-target-comment-container .image-owner-target-container .arrow-container{position:relative;float:left}.image-owner-target-comment-container .image-owner-target-container .arrow-container .arrow{float:left;height:24px;margin-top:8px;opacity:0.65}.image-owner-target-comment-container .image-owner-target-container .target-name{float:left;display:inline-block;margin-right:12px}
	</style>
</head>
<body>
<div class="all-comment-container">
	<div class="index" style="background-color: #fafafa;padding: 12px;">
		<div>
			<h2 style="margin:0;">Index</h2>
		</div>
		{users}
			<div class="profile-link-container" data-name="{fullname}">
				<a class="capitalize fullname" href="#id-{id}">{fullname}</a>
			</div>
		{/users}
	</div>
	<div>
		{users}
			<div class="person-data-container" id="id-{id}" data-id="{id}">
				<div class="profile-container">
					<div class="avatar-container">
						<img class="avatar" src="./e/img/default-user.jpg" data-src="{avatar}">
					</div>
					<div class="profile-detail-container">
						<div>
							<span class="name capitalize colorfull">{fullname}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Nickname: </span>
							<span class="if-statement">{nickname}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Email: </span>
							<span class="if-statement">{alternate_email}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Phone No.: </span>
							<span class="if-statement">{phone}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Roll No.: </span>
							<span class="if-statement">{roll}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Department: </span>
							<span class="if-statement">{department}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Address: </span>
							<span class="if-statement">{address}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Fondly known for: </span>
							<span class="if-statement">{fondly_known_for}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Favourite hangout place in campus: </span>
							<span class="if-statement">{favourite_hangout_place_in_campus}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Favourite professor: </span>
							<span class="if-statement">{favourite_professor}</span>
						</div>
						<div class="if-container">
							<span class="colorfull">Most memorable moment in campus: </span>
							<span class="if-statement">{most_memorable_moment_on_campus}</span>
						</div>
						<div class="aboutme if-container">
							<span class="colorfull">About me: </span>
							<span class="if-statement">{about}</span>
						</div>
					</div>
				</div>
			{comments}
				<div class="image-owner-target-comment-container">
					<div class="image-owner-target-container">
						<a href="#id-{ownerid}" class="image-container">
							<img src="./e/img/default-user.jpg" data-src="{image}" class="owner-image">
						</a>
						<a href="#id-{ownerid}" class="owner-name capitalize">{owner}</a>
						<span class="arrow-container">
							<img class="arrow" src="e/img/arrow_right_black.png">
						</span>
						<span class="target-name capitalize" >{target}</span>
					</div>
					<div>{comment}</div>
				</div>
			{/comments}
			</div>
		{/users}
	</div>
</div>
<div>
	<div>
		Created by Amit Saharan with love.
	</div>
</div>
<script type="text/javascript">
	function check_for_empty_stuff(){
		var ifContainers = document.querySelectorAll('.if-container');
		var ifContainersToHide = [];
		var i = 0;
		for(i=0; i < ifContainers.length; i++){
			var ifContainer = ifContainers[i];
			var ifStatement = ( ifContainer.lastElementChild.innerHTML == '' );
			if(ifStatement){
				ifContainersToHide.push(ifContainer);
			}
		}
		for(i=0; i < ifContainersToHide.length; i++){
			ifContainersToHide[i].setAttribute('data_force_hide','true');
		}
	}
	
	var images_to_load = document.querySelectorAll('[data-src]');
	var images_remaining = images_to_load.length;
	var total_images = images_remaining;

	function load_all_images(how_many){
		console.log('load ',how_many,' images')
		for(var i=0; i < how_many && images_remaining > 0; i++){
			var image_to_load = images_to_load[images_to_load.length - images_remaining];
			images_remaining -= 1;
			image_to_load.setAttribute('src',image_to_load.getAttribute('data-src'));
		}
	}

	function beutifyIndex() {
		var nameContainers = document.querySelectorAll('.profile-link-container');
		var previousChar = '';
		for(var i=0; i < nameContainers.length; i++){
			var nameContainer = nameContainers[i];
			var name = nameContainer.getAttribute('data-name');
			if(name[0]!=previousChar){
				var div = document.createElement('div');
				div.innerHTML=name[0];
				previousChar = name[0];
				div.setAttribute('class','name-first-char');
				nameContainer.parentNode.insertBefore(div,nameContainer)
			}
		}
	}

	function load_images_on_scroll(){
		if(images_remaining > 100){
			var how_many_to_load = window.scrollY/document.body.scrollHeight * 1.1 * total_images - ( total_images - images_remaining );
			if(how_many_to_load > 0){
				load_all_images( how_many_to_load );
			}
		}else{
			if(images_remaining>0){
				load_all_images(images_remaining);
			}
		}
	}
	window.addEventListener('scroll',load_images_on_scroll);
	
	// beutifyIndex();
	check_for_empty_stuff();
	window.addEventListener('load',function(){
			load_images_on_scroll();
	});
</script>
</body>
</html>