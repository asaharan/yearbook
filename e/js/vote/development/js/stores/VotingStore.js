var AppDispatcher = require('../dispatcher/AppDispatcher');
var EventEmitter = require('events');
var Constants = require('../constants/Constants');
var makeTree=require('../lib/makeTree');
var assign = require('object-assign');
var $=require('jquery');
var Toaster = require('../../lib/Toast.react');

var CHANGE_EVENT = 'change';
var relativeApi='bestProfessor/';

var votingStats=[];
var search={};
search.q='';
search.active=false;
search.result=[];
var lastTime='';
var myVote=0;
var showAllFlag=false;
var normalTimeLag=5;
var timeLag=normalTimeLag;
var maxTimeLag=25;
var totalVotes=0;

function searchQuery(q,listToSearch){
	var a=[];
	listToSearch.map(function(e){
		var t=false,mc=0,nmc=0;
		var p=e.name;
		q=q.trim();
		var s=q.split(' ');
		var priority=0;
		if(s.length==1&&e.e==q){
			t=true;
			priority=9;
		}else{
			var indexOfName = p.search(new RegExp(q,'i'));
			if(indexOfName !=-1 ){
				if(indexOfName==0){
					priority=8;
				}else{
					priority=7;
				}
				t=true;
			}else{
				s.map(function(k){
					if(k==''||k==null||k.length<3){
						return;
					}
					var w=p.search(new RegExp(k,'i'));
					if(w!=-1){
						mc++;
						return;
					}else{
						nmc++;
					}
				});
			}
		}
		if(nmc<mc){
			t=true;
		}
		e.priority=priority;
		if(t){
			a.push(e);
		}
	});
	a.sort(function(e,d){return d.priority - e.priority;});
	for(var i=0;i<a.length;i++){
		// a[i].priority=0;
		delete a[i]['priority']
	}
	return a;
}

var VotingStore = assign({}, EventEmitter.prototype, {
	emitChange:function(){
		this.emit(CHANGE_EVENT);
	},
	addChangeListener:function(callback){
		this.on(CHANGE_EVENT,callback);
	},
	removeChangeListener:function(callback) {
	    this.removeListener(CHANGE_EVENT, callback);
	},
	init:function(){
		this.updateStats();
	},
	getVotingStats:function(){
		if(search.active){
			return search.result;
		}
		return votingStats;
	},
	updateStats:function(){
		var self=this;
		$.ajax({
			method:'GET',
			url:relativeApi+'stats/',
			dataType:'json',
			data:{after:lastTime},
			success:function(r){
				lastTime=r.last_time;
				myVote=r.my_vote;
				totalVotes=r.total_votes;
				if(r.newVotes!=undefined && r.newVotes!='' && typeof r.newVotes =='object' && r.newVotes!=null){
					for(var i=0;i<r.newVotes.length;i++){
						var s=r.newVotes[i].name + ' received '+r.newVotes[i].votes+' new ';
						s+= r.newVotes[i].votes>1? 'votes' : 'vote.';
						// Toaster.make(s,3000);
					}
				}

				if(r.update){
					votingStats=r.data;
					timeLag=normalTimeLag;
				}else{
					timeLag*2;
					if(timeLag>maxTimeLag){
						timeLag=maxTimeLag;
					}
				}
				if(search.active){
					self.searchQueryChanged(search.q);
				}
				self.emitChange();
			},
			error:function(){
				self.emitChange();
			}
		});
	},
	getTotalVotes:function(){
		return totalVotes;
	},
	getTimeLag:function(){
		return timeLag;
	},
	getMyVote:function(){
		return myVote;
	},
	voteFor:function(id, clear){
		var toastId=Toaster.make('',{type:'loading',loading:{style:{margin:0}}});
		var self=this;
		if(clear!=true){
			clear='';
		}
		$.ajax({
			method:'GET',
			url:relativeApi+'vote',
			data:{profid:id,clear_vote:clear},
			dataType:'json',
			success:function(r){
				Toaster.clear(toastId);
				self.updateStats();
			},
			error:function(){
				Toaster.clear(toastId);
			}
		});
	},
	searchQueryChanged:function(q){
		search.q=q;
		if(q==''){
			search.active=false;
		}else{
			search.active=true;
		}
		search.result=searchQuery(q,votingStats);
		this.emitChange();
	},
	getShowAllFlag:function(){
		return showAllFlag;
	},
	updateShowAllFlag:function(flag){
		showAllFlag=flag;
		this.emitChange();
	}
});

AppDispatcher.register(function(action) {
	switch(action.actionType) {
		case Constants.INIT:
			VotingStore.init();
			break;
		case Constants.UPDATE_STATS:
			VotingStore.updateStats();
			break;
		case Constants.VOTE_FOR:
			VotingStore.voteFor(action.id,action.clear);
			break;
		case Constants.SEARCHQUERY_CHANGED:
			VotingStore.searchQueryChanged(action.q);
			break;
		case Constants.UPDATE_SHOWALLFLAG:
			VotingStore.updateShowAllFlag(action.flag);
			break;
	};
});
module.exports=VotingStore;