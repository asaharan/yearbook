<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function index(){
		$this->load->helper('login');
		// $this->load->helper('url');
		if(isloggedin()){
			$username=getusername();
			if($username==false){
				redirect('/login/', 'refresh');
			}else{
				redirect('/profile/'.$username, 'refresh');
			}
		}else{
			redirect('/login/', 'refresh');
		}
	}
	public function main($targetUsername){
		$this->load->helper('login');
		if(!isloggedin()){
			redirect('/login/','refresh');
			return;
		}
		$userid=userid();
		$targetUsername = strtolower($targetUsername);
		$ownUsername=getusername();
		$ownProfile=$targetUsername==$ownUsername;
		if(userexists($targetUsername)){
			$targetInfo=userinfo($targetUsername);
			if($ownProfile){
				$this->db->select('comments.id, comments.preference, comments.targetId, comments.comment, comments.timestamp, users.username, users.name, users.avatar');
				$this->db->join('users','users.id=comments.ownerId');
				$pendingComments=$this->db->get_where('comments',array('targetId'=>$userid,'visible'=>1,'choosen'=>0))->result_array();


				$this->db->select('comments.id, comments.preference, comments.targetId, comments.comment, comments.timestamp, users.username, users.name, users.avatar');
				$this->db->join('users','users.id=comments.ownerId');
				$this->db->order_by('preference asc,id asc');
				$choosenComments=$this->db->get_where('comments',array('targetId'=>$userid,'visible'=>1,'choosen'=>1))->result_array();


				$this->load->view('ownProfile',array('username'=>$targetUsername,'info'=>$targetInfo,'choosenComments'=>$choosenComments,'pendingComments'=>$pendingComments));
			}else{
				$x=$this->db->get_where('comments',array('targetId'=>$targetInfo->id,'ownerId'=>$userid,'visible'=>1),1)->result_array();
				$alreadyWritten=false;
				if(sizeof($x)>=1){
					$alreadyWritten=true;
					$x=$x[0];
				}
				$this->load->view('othersProfile',array('username'=>$targetUsername,'info'=>$targetInfo,'comment'=>$x,'alreadyWritten'=>$alreadyWritten));
			}
		}else{
			$this->load->view('errors/html/userNotExist');
		}
	}
}