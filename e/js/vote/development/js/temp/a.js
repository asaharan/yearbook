function makeTree(node,rows){
    for(var i=0;i<node.length;i++){
        for(var j=0;j<rows.length;j++){
            if(node[i].id==rows[j].parent_id && node[i].id!=rows[j].id){
                node[i].childNodes.push(rows[j]);
                node[i].childNodes[node[i].childNodes.length - 1].childNodes=[];

                makeTree(node[i].childNodes,rows);
            }
        }
    }
}
var TreeMaker={}
TreeMaker.makeTree=function(rows){
    var Tree=[];
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];

        if(row.id==row.parent_id){
            Tree.push(row);
            Tree[Tree.length-1].childNodes=[];
        }
    }
    makeTree(Tree,rows);
    return Tree;
}
module.exports=TreeMaker;