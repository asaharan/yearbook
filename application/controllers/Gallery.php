<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends CI_Controller {
	public function index()
	{
		$this->load->helper('login');
		if(!isloggedin()){
			redirect('/','refresh');
		}
		$userid=userid();
		$this->db->select('id,path');
		$this->db->order_by('id','desc');
		$r=$this->db->get_where('gallery',array('user_id'=>$userid,'deleted'=>0))->result_array();
		$this->load->view('gallery',array('data'=>$r));
	}
}