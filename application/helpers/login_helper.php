<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	function isloggedin(){
		$CI=&get_instance();

		if(empty($CI->input->cookie('skill_user'))||empty($CI->input->cookie('skill_token'))){
			return false;
		}
		$user_id=$CI->input->cookie('skill_user');
		$login_key=$CI->input->cookie('skill_token');
		$credentials=array('user_id'=>$user_id,'token'=>$login_key,'valid'=>1);
		$CI->db->select('valid');
		$CI->db->where($credentials);
		$loginCheck=$CI->db->count_all_results('login');
		if($loginCheck==1){
			return true;
		}
		return false;
	}

	function getusername(){
		$uid=userid();
		if($uid==false){
			return false;
		}
		$CI=&get_instance();
		$r=$CI->db->get_where('users',array('id'=>$uid))->result_array();
		if(sizeof($r)>0){
			return $r[0]['username'];
		}
		return false;
	}

	function login($username,$p1){
		$CI=&get_instance();
		if(empty($username)){
			return false;
		}
		$login=match_password($username,$p1);
		if(!$login){
			return false;
		}
		$user=usernametoid($username);
		$credentials=array(
			'name'=>'user',
			'value'=>$user,
			 'expire' => '2592000',
			  'prefix' => 'skill_',
			  'path'=>'/'
		);
		$CI->load->helper('cookie');
		$CI->input->set_cookie($credentials);

		$token=generateRandomString(40);
		$tokeS=array('user_id'=>$user,'token'=>$token);
		$CI->db->insert('login',$tokeS);

		$credentials['name']='token';
		$credentials['value']=$token;
		$CI->input->set_cookie($credentials);

		return true;
	}

	function logout(){
		$CI=&get_instance();
		$token=$CI->input->cookie('skill_token');
		$user_id=$CI->input->cookie('skill_user');
		$CI->input->set_cookie(array('name'=>'skill_user','value'=>0,'expire'=>''));
		$CI->input->set_cookie(array('name'=>'skill_token','value'=>0,'expire'=>''));
		if(empty($token)||empty($user_id)){
			return false;
		}
		$credentials=array('user_id'=>$user_id,'token'=>$token);
		$toupdate['valid']=0;
		$CI->db->where($credentials)->update('login',$toupdate);
		return true;
	}

	function logout_every_where($id=0){
		if(!isloggedin() && $id==0){
			return false;
		}
		$CI=&get_instance();
		if($id==0){
			$id=userid();
		}
		$CI->db->where(array('user_id'=>$id));
		$CI->db->update('login',array('valid'=>0));
		// echo $CI->db->affected_rows();
		return $id;
	}

	function match_password($username,$p1,$id=0){
		$CI=&get_instance();
		if(empty($username)){
			return false;
		}
		$given='username';

		if($id==1){
			$given='id';
		}
		$CI->db->where(array('confirm'=>1));
		$password_hash=$CI->db->select('password')->where($given,$username)->get('users');
		if($password_hash->num_rows()!=1){
			return false;
		}
		$password_hash=$password_hash->row()->password;

		return password_verify($p1,$password_hash);
	}

	function signup($username,$name,$signuptoken,$gender="M",$college="@iitk.ac.in",$hostel,$department,$roll,$phone){
		$CI=&get_instance();

		if(empty($username)||empty($name)){
			return false;
		}

		if ((strpos($username,'@') !== false)) {
			return false;   
		}
		$username=strtolower($username);
		$CI->db->where('username',$username);
		if($CI->db->count_all_results('users')==1){
			return false;
		}
		$confirm_token=generateRandomString(10);
		$password=hash_password($signuptoken);
		$credentials=array('username'=>$username,'password'=>$password,'name'=>$name,'college'=>$college,'confirm_token'=>$confirm_token,'gender'=>$gender,'hostel'=>$hostel,'department'=>$department,'roll'=>$roll,'avatar'=>'e/img/default-user.jpg');
		$CI->db->insert('users',$credentials);
		if($CI->db->affected_rows()!=1){
			return false;
		}
		$id=$CI->db->insert_id();
		sendmail($username.$college,'signup',$name,$username,$id,$confirm_token);
		return true;
	}

	function sendmail($email,$topic,$name,$username,$techid,$confirm_token){
		$CI=&get_instance();
		$CI->load->library('email');
		$msg="";
		$config['mailtype']='text';
		$config['smtp_host']='sg2plcpnl0153.prod.sin2.secureserver.net';
		$config['smtp_port']='465';
		$config['smtp_user']='aprevents2016';
		$config['smtp_pass']='Amit-Saharan1';
		$config['smtp_timeout']='60';
		$CI->email->initialize($config);
		if($topic=='signup'){
			$CI->email->from('no-reply@aprevents.com','Team APR');
			$CI->email->to($email); 
			$CI->email->subject('Yearbook');
			$msg.="Welcome $name\n\rGreetings from Team APR.\n\r";
			$msg.="Your Username is $username\n\r";
			$msg.="Click the link below to verify your email.\n\r".base_url()."login/confirm_email?i=$techid&e=$username&t=$confirm_token\n\r";
			$msg.="Only after confirmation you will be able to login.\n\r";
			$msg.="Visit ".base_url()." to explore about competitions, workshops, exhibitions and much more.\n\r";
			//$msg.="<br>To get latest updates follow us on <a href='https://facebook.com/techkriti.iitk'>Facebook</a>.";
			$CI->email->message($msg);
			$CI->email->send();
		}else{
			if($topic=='forgot_password'){
				$CI->email->from('no-reply@aprevents.com','Team APR');
				$CI->email->to($email); 
				$CI->email->subject('Yearbook');
				$msg.="Follow the link below to update your password\n\r";
				$msg.=base_url()."login/update_forgot_password?i=$techid&e=$username&t=$confirm_token";
				$CI->email->message($msg);
				$CI->email->send();
			}
		}
	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZSkillScrollAmitSaharan';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	function userid(){
		$CI=&get_instance();
		if(empty($CI->input->cookie('skill_user'))){
			return false;
		}

		return $CI->input->cookie('skill_user');
	}

	function usernametoid($username){
		$CI=&get_instance();

		$result=$CI->db->select('id')->where('username',$username)->get('users')->result_array();
		if(empty($result)){
			return false;
		}
		return $result[0]['id'];
	}

	function userinfo($username){
		$CI=&get_instance();
		if(empty($username)){
			return false;
		}

		$result=$CI->db->select('id,username,about,department,roll,hostel,alternate_email,name,email,phone,facebook,address,college,avatar,nickname,about,fondly_known_for,favourite_hangout_place_in_campus,donation_amount,favourite_professor,most_memorable_moment_on_campus')->get_where('users',array('username'=>$username));
		if($result->num_rows()!=1){
			return false;
		}

		return $result->row();
	}

	function get_last_chars_of_pass($pass){
		return substr($pass, sizeof($pass) - 21 );
	}

	function send_update_password_mail($id){
		$CI=&get_instance();
		if(empty($id)){
			return false;
		}
		$r=$CI->db->get_where('users',array('id'=>$id))->result_array();
		if(sizeof($r)==0){
			return false;
		}
		$r=$r[0];
		// $p=substr($r['password'], sizeof($r['password']) - 21 );
		$p=get_last_chars_of_pass($r['password']);
		sendmail($r['username'].$r['college'],'forgot_password',$r['name'],$r['username'],$r['id'],$p);
		return true;
	}

	function updatepassword($user_id,$old_pass,$new_pass,$forgot=false){
		$CI=&get_instance();
		if(!match_password($user_id,$old_pass,1) && $forgot==false){
			return false;
		}
		$password=hash_password($new_pass);

		$credentials=array('password'=>$password);
		$CI->db->where('id',$user_id)->update('users',$credentials);
		return true;
	}

	function hash_password($pass){
		$salt=generateRandomString(24);
		return password_hash($pass,PASSWORD_BCRYPT,array('salt'=>$salt));
	}

	function RegisterMe($event,$teamName,$admin,$ids,$zonal){//register for competitions
		$CI=&get_instance();
		$toInsert=array('title'=>$teamName,'event'=>$event,'admin_id'=>$admin);
		if(!empty($zonal)){
			$toInsert['zonal']=$zonal;
		}
		$r=$CI->db->insert('groups',$toInsert);
		if($CI->db->affected_rows()==0){
			return false;
		}
		$group_id=$CI->db->insert_id();
		for($i=0;$i<sizeof($ids);$i++){
			if(empty($ids[$i])){
				continue;
			}
			$CI->db->insert('group_users',array('group_id'=>$group_id,'user'=>$ids[$i]));
		}
		return true;
	}

	function RegisterWorkshop($event,$teamName,$admin,$ids,$zonal){//register for workshop
		$CI=&get_instance();
		$toInsert=array('title'=>$teamName,'workshop'=>$event,'admin_id'=>$admin);
		if(!empty($zonal)){
			$toInsert['zonal']=$zonal;
		}
		$r=$CI->db->insert('workshop_groups',$toInsert);
		if($CI->db->affected_rows()==0){
			return false;
		}
		$group_id=$CI->db->insert_id();
		for($i=0;$i<sizeof($ids);$i++){
			if(empty($ids[$i])){
				continue;
			}
			$CI->db->insert('workshop_group_users',array('group_id'=>$group_id,'user'=>$ids[$i]));
		}
		return true;
	}

	function RegisterWorkshopInCampus($workshop,$teamName,$admin,$ids){//register for workshop
		$CI=&get_instance();
		$toInsert=array('title'=>$teamName,'workshop'=>$workshop,'admin_id'=>$admin);
		$r=$CI->db->insert('in_campus_workshop_groups',$toInsert);
		if($CI->db->affected_rows()==0){
			return false;
		}
		$group_id=$CI->db->insert_id();
		for($i=0;$i<sizeof($ids);$i++){
			if(empty($ids[$i])){
				continue;
			}
			$CI->db->insert('in_campus_workshop_group_users',array('group_id'=>$group_id,'user'=>$ids[$i]));
		}
		return true;
	}

	function RegisterForEvent($detail){
		$CI=&get_instance();
		$CI->db->insert('register',array('event'=>$detail['event'],'zonal'=>$detail['zonal'],'leader_id'=>$detail['leader']->id));
		$reg_id=$CI->db->insert_id();
		for ($i=0; $i < sizeof($detail['names']); $i++) {
			$data=[];
			$data['reg_id']=$reg_id;
			if(isset($detail['names'][$i])){
				$data['name']=$detail['names'][$i];
			}
			if(isset($detail['emails'][$i])){
				$data['email']=$detail['emails'][$i];
			}
			if(isset($detail['facebooks'][$i])){
				$data['facebook']=$detail['facebooks'][$i];
			}
			if(isset($detail['phones'][$i])){
				$data['phone']=$detail['phones'][$i];
			}
			$CI->db->insert('reg_user',$data);
		}
		registerMail($detail['leader']->name,$detail['leader']->email,$detail['event']);
		return true;
	}

	function registerMail($name,$email,$event){
		$CI=&get_instance();
		$CI->load->library('email');
		$msg="You have successfully registred for $event.\n";
		
		$CI->email->from('techkriti@iitk.ac.in','Techkriti IIT Kanpur');
		$CI->email->to($email); 
		$CI->email->subject("Registration successful");
		$msg.="Visit Techkriti at http://techkriti.org for more information";
		$CI->email->message($msg);
		$CI->email->send();
	}

	function usernameavailable($u){
		$CI=&get_instance();
		$r=$CI->db->get_where('users',array('username'=>$u));
		if($r->num_rows()>0){
			return false;
		}
		return true;
	}

	function userexists($u){
		return !usernameavailable($u);
	}

	function emailavailable($u){
		$CI=&get_instance();
		$r=$CI->db->get_where('users',array('email'=>$u));
		if($r->num_rows()>0){
			return false;
		}
		return true;
	}

	function giveData($uid){
		$CI=&get_instance();
		$r=[];
		$X=$CI->db->get_where('register',array('leader_id'=>$uid))->result_array();
		for($i=0;$i<sizeof($X);$i++){
			$r['users'][$X[$i]['id']]=$CI->db->get_where('reg_user',array('reg_id'=>$X[$i]['id']))->result_array();
		}
		$r['events']=$X;
		return $r;
	}

	function confirm($username,$techid,$confirm_token){
		$CI=&get_instance();
		$r=$CI->db->get_where('users',array('username'=>$username,'id'=>$techid,'confirm_token'=>$confirm_token));
		if($r->num_rows()>0){
			$CI->db->where(array('id'=>$techid));
			$CI->db->update('users',array('confirm'=>1));
			return true;
		}
		return false;
	}

	function update_dpflag($id){
		$CI=&get_instance();
		$CI->db->where(array('id'=>$id));
		$CI->db->update('users',array('dp_flag'=>1));
	}