<!DOCTYPE html>
<html>
<head>
	<title>Login | Yearbook</title>
	<base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="e/css/main.css">
</head>
<body>
<div class="container" style="text-align: center;">
	<form autocomplete="off" action="<?php echo base_url();?>login/register" method="post" style="max-width: 600px;text-align: left;display: inline-block;width: 400px;background: #fff;padding: 10px;margin-top: 12%;">
		<div class="input-field col s6">
			<input autocomplete="off" type="text" name="username" class="validate">
			<label for="first_name">IITK username</label>
        </div>
        <div class="input-field col s6">
			<input autocomplete="off" type="text" name="name" class="validate">
			<label for="first_name">Name</label>
        </div>
        <div class="input-field col s6">
        	<input autocomplete="off" type="text" name="alternate_email" class="validate">
        	<label for="first_name">Alternate email address</label>
        </div>
        <div class="input-field col s6">
        	<input autocomplete="off" type="text" name="phone" class="validate">
        	<label for="first_name">Phone</label>
        </div>
        <div class="input-field col s6">
        	<input autocomplete="off" type="text" name="roll" class="validate">
        	<label for="first_name">Roll No.</label>
        </div>
        <div class="input-field col s6">
        	<input autocomplete="off" type="text" name="department" class="validate">
        	<label for="first_name">Department</label>
        </div>
        <div class="input-field col s6">
        	<input autocomplete="off" type="text" name="hostel" class="validate">
        	<label for="first_name">Hostel</label>
        </div>
        <div class="input-field col s6">
			<input type="password" name="password" class="validate">
			<label for="first_name">Password</label>
        </div>
        <div class="row">
			<button class="col push-s1 s4 waves-effect waves-light btn" type="submit">Sign Up</button>
			<a class="col s4 push-s2 waves-effect waves-light btn" href="<?php base_url() ?>login">Login</a>
		</div>
	</form>
</div>
<script type="text/javascript" src="e/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="e/css/materialize/js/materialize.min.js"></script>
</body>
</html>