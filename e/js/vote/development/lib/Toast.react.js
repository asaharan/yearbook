var React = require('react');
var $=require('jquery');
var ReactDOM = require('react-dom');
var Dispatcher = require('flux').Dispatcher;
var EventEmitter = require('events');
var assign = require('object-assign');
var keyMirror = require('keymirror');
var Loading = require('./utils/Loading');

var CHANGE_EVENT='change';
var ToastConstants = keyMirror({
  INIT:null,
  ADD_TOAST:null,
  REMOVE:null,
  REMOVE_ALL:null,
});

var ToastDispatcher = new Dispatcher();
var ToastActions={};
var _ID=0;
var ReactRoot=null;
var data=[];
var totalVisibleCount=0;

var ToastStore = assign({}, EventEmitter.prototype, {
	addChangeListener:function(callback){
		this.on(CHANGE_EVENT,callback);
	},
	emitChange:function(){
		this.emit(CHANGE_EVENT);
	},
	init:function(){
	},
	add:function(e){
		data.push(e);
		totalVisibleCount+=1;
		this.emitChange();
		if( data.length > 100 ){
			this.optimise();
		}
	},
	getData:function(){
		return data;
	},
	getTotalVisibleCount:function(){
		return totalVisibleCount;
	},
	optimise:function(){
		for (var i = 0; i < data.length; i++) {
			if(data[i] && data[i].visible==false){
				delete data[i];
			}
		}
	},
	remove:function(id){
		for(var i=0;i<data.length;i++){
			if( data[i] && data[i].id==id && data[i].visible!=false){
				totalVisibleCount-=1;
				data[i].visible=false;
				break;
			}
		}
	},
	removeAll:function(){
		data=[];
		totalVisibleCount=0;
	},
	removeChangeListener:function(callback) {
	    this.removeListener(CHANGE_EVENT, callback);
	},
});

var Toast = React.createClass({
	getInitialState:function(){
		return {bottom:-100,spacing:15,height:52,timeoutId:0,width:0};
	},
	componentDidMount:function(){
		var self=this;
		var i=requestAnimationFrame(function(){
			var bottom=self.props.position*( self.state.spacing + self.state.height ) + self.state.spacing;
			self.setState({bottom: bottom });
		});
		this.setState({timeoutId:i});
		this.setState({width:this.width()});
	},
	width:function(){
		var DOMNode=ReactDOM.findDOMNode(this.refs.q);
		return $(DOMNode).innerWidth();;
	},
	componentWillReceiveProps:function(){
		var bottom=this.props.position*( this.state.spacing + this.state.height ) + this.state.spacing;
		this.setState({bottom: bottom });
	},
	componentDidReceiveProps:function(){
		this.setState({width:this.width()});
		// console.log('componentDidReceiveProps');
	},
	componentWillUnmount:function(){
		cancelAnimationFrame(this.state.timeoutId);
	},
	componentDidUpdate:function(){
		var bottom=this.props.position*( this.state.spacing + this.state.height ) + this.state.spacing;
		if(bottom!=this.state.bottom){
			this.setState({bottom: bottom });
		}
	},
	render:function(){
		var s={
			position:'fixed',
			left:this.state.spacing,
			backgroundColor: '#404040',
			color:'#fff',
			bottom: this.state.bottom,
			borderRadius: 3,
			padding: 16,
			boxShadow: '0 0 2px rgba(0,0,0,.12),0 2px 4px rgba(0,0,0,.24)',
			lineHeight:'20px',
			transition:'bottom ease 300ms',
		};
		if(this.props.innerWidth < this.state.width + 30 ){
			s.right=15;
		}
		var mainText=(<span>{this.props.data.text}</span>);/**/
		var postExtra=null,preExtra=null;
		if(this.props.data.type=='loading'){
			preExtra=(<Loading data={this.props.data.loading}/>);
		}
		///
		return (
			<div ref="q" style={s}>
				{preExtra}
				{mainText}
				{postExtra}
			</div>
			///
		);
	}
});
var ToastContainer = React.createClass({
	_onChange:function(){
		this.setState({
			data:ToastStore.getData(),
			totalVisibleCount:ToastStore.getTotalVisibleCount()
		});
	},
	componentDidMount:function(){
		ToastStore.addChangeListener(this._onChange);
		var self=this;
		var b=function(){
			self.setState({innerWidth:window.innerWidth});
		}
		b();
		window.addEventListener('resize',b);
	},
	getInitialState:function(){
		return {data:ToastStore.getData(),innerWidth:1366};
	},
	render:function(){
		var self=this;
		var p=0;
		var a=this.state.data.map(function(b){
			if(b.visible==false){
				return;
			}
			p+=1;
			// console.log(b.text,totalVisibleCount - p )
			return (<Toast innerWidth={self.state.innerWidth} position={ totalVisibleCount - p} key={b.id} data={b} />);///
		});
		return(
			<div>
				{a}
			</div>
		);
		///
	}
});
ToastActions={
	make:function(text,options){
		_ID++;
		var self=this;
		var d={id:_ID,text:text};
		if(options){
			if(isNaN(options)){
				d=assign(d,options);
				// console.log(d);
				if( (!isNaN(options.timeout)) && options.timeout>0){
					setTimeout(function(){self.clear(d.id)},options.timeout);	
				}
			}else{
				setTimeout(function(){self.clear(d.id)},options);
			}
		}
		ToastDispatcher.dispatch({
			actionType:ToastConstants.ADD_TOAST,
			data:d
		});
		return _ID;
	},
	clear:function(id){
		ToastDispatcher.dispatch({
			actionType:ToastConstants.REMOVE,
			id:id
		});
	},
	clearAll:function(){
		ToastDispatcher.dispatch({
			actionType:ToastConstants.REMOVE_ALL
		});
	}
};

(function(){
	var a=document.getElementById('b-toast-holder');
	if(!a){
		a=$('<div id="b-toast-holder">');
		$('body').append(a);
		a=document.getElementById('b-toast-holder');
	}
	ReactRoot=a;
	ReactDOM.render(
		<ToastContainer/>,
		a
	);
	///
})();

ToastDispatcher.register(function(action){
	switch(action.actionType){
		case ToastConstants.INIT:
			ToastStore.init();
			break;
		case ToastConstants.ADD_TOAST:
			ToastStore.add(action.data);
			break;
		case ToastConstants.REMOVE:
			ToastStore.remove(action.id);
			ToastStore.emitChange();
			break;
		case ToastConstants.REMOVE_ALL:
			ToastStore.removeAll();
			ToastStore.emitChange();
			break;
	}
})
module.exports=ToastActions;