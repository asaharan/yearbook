var keyMirror = require('keymirror');

module.exports = keyMirror({
	INIT:null,
	UPDATE_STATS:null,
	VOTE_FOR:null,
	SEARCHBOX_FOCUSED:null,
	SEARCHQUERY_CHANGED:null,
	UPDATE_SHOWALLFLAG:null,
});