var React = require('react');
var $ = require('jquery');
var CreateResume = React.createClass({
	render:function(){
		return(
			<div>
				<div>Let's Create a new Resume</div>
				<StartDetailForm/>
			</div>
			/**/
		);
	}
});

var StartDetailForm=React.createClass({
	getInitialState:function(){
		return {name:'',email:'',phone:''};
	},
	render:function(){
		return (
			<div>
			<div>{this.state.name}{this.state.email}{this.state.phone}</div>
			<label>
				<legend>Name</legend>
				<input type="text" data-varname="name" name="name" onChange={this._onChange} ref="name"/>
			</label>
			<label>
				<legend>Email</legend>
				<input type="text" data-varname="email" name="email" onChange={this._onChange} ref="email"/>
			</label>
			<label>
				<legend>Phone No</legend>
				<input type="text" data-varname="phone" name="phone" onChange={this._onChange} ref="phone"/>
			</label>
			<button onClick={this._onSubmit}>Create</button>
			</div>
			/**/
		);
	},
	_onChange:function(e){
		var val=e.target.value;
		var varname=e.target.attributes['data-varname'].nodeValue;
		this.setState({[varname]:val});
	},
	_onSubmit:function(){
		$.ajax({
			method:'PUT',
			url:'api/resume/',
			data:this.state,
			success:function(r){
				if(r.success==true){
					console.log(r);
				}else{
					console.log(r.error);
				}
			}
		});
	}
})

module.exports = CreateResume;