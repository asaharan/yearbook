<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Yearbook extends CI_Controller {
	public function index()
	{
		$this->load->helper('login');
		$this->load->library('parser');
		$r=[];
		$r['success']=FALSE;
		$userid=userid();
		if(!isloggedin()){
			$userid=0;
		}

		$this->db->limit(50);
		$this->db->select('*,LOWER(name) as fullname');
		$this->db->order_by('name');
		$users = $this->db->get_where('users',array('confirm'=>1,'inlist'=>1))->result_array();
		// echo sizeof($users);
		for($i=0; $i < sizeof($users); $i++){
			$user = $users[$i];

			$this->db->select('comments.comment, LOWER(ownertable.name) as owner, ownertable.id as ownerid, ownertable.avatar as image, "'.$user['name'].'" as target ');
			$this->db->join('users as ownertable','ownertable.id = comments.ownerid');
			// $this->db->join('users as target','target.id = comments.targetid');
			$this->db->where(array('visible'=>1, 'choosen' => 1, 'targetid'=>$user['id']));
			$this->db->order_by('owner,preference');
			$users[$i]['comments'] = $comments = $this->db->get('comments')->result_array();
		}
		
		// $this->db->select('targetid, ownerid,comments.comment, owner.name as owner, target.name as target, owner.avatar as image');
		// $this->db->join('users as owner','owner.id = comments.ownerid');
		// $this->db->join('users as target','target.id = comments.targetid');
		// $this->db->where(array('visible'=>1, 'choosen' => 1));
		// $this->db->order_by('targetid,preference');
		// $this->db->limit(50);
		// $comments = $this->db->get('comments')->result_array();

		$data = array('users'=>$users);
		$this->parser->parse('yearbook', $data);
		// $this->load->view('yearbook',$data);
	}
	public function gallery(){
		// $this->db->limit(50);
		$this->db->select('gallery.*, u.name, u.confirm, u.inlist');
		$this->db->join('users as u','u.id = gallery.user_id');
		$this->db->order_by('u.name');
		$gallery = $this->db->get_where('gallery',array('confirm'=>1,'inlist'=>1, 'deleted'=>0))->result_array();
		echo json_encode($gallery);
	}
}