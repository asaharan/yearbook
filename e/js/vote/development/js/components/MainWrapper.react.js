var React = require('react');
var ReactDOM = require('react-dom');
var VotingActions=require('../actions/VotingActions');
var VotingStore=require('../stores/VotingStore');
var Toaster = require('../../lib/Toast.react');

var Cross=React.createClass({
	render:function(){
		var size=24;
		if(this.props.size!=undefined){
			size=this.props.size;
		}
		var a={position:'absolute',width:size,height:size};
		a.opacity=this.props.active?1:0;
		var w=a.width / 2;
		var top = ( size - w ) / 2;

		var b={position:'absolute',top:top,left:top,width:w,height:2,transform:'rotate(-45deg)',backgroundColor:'#ccc'};
		var c=JSON.parse(JSON.stringify(b));
		c.transform='rotate(45deg)';
		return(
			<span style={a} className="transition-300">
				<span style={b}></span>
				<span style={c}></span>
			</span>
		);
		///
	}
});

var SearchBox=React.createClass({
	getInitialState:function(){
		return {active:false};
	},
	render: function() {
		var iconSearchMorphStyle={position:'absolute',top:15,left:22,width:24,height:24,cursor:'pointer'}
		
		if(!this.state.focus){
			iconSearchMorphStyle.zIndex=10;
		}else{
			iconSearchMorphStyle.zIndex=50;
		}

		var iconCrossMorphStyle=JSON.parse(JSON.stringify(iconSearchMorphStyle));
		iconCrossMorphStyle.right=iconCrossMorphStyle.left;
		iconCrossMorphStyle.zIndex=400;
		iconCrossMorphStyle.paddingTop=11;
		delete iconCrossMorphStyle['left'];
		return (
			<div className="SearchBox">
				<input autoFocus="true" className="searchBox" placeholder="Search" ref="searchInput" onFocus={this.onFocus} onBlur={this.onBlur} type="text" onChange={this.onValueChange} />
				<a onClick={this.clearInputField} style={iconCrossMorphStyle}>
					<Cross active={this.state.active}/>
				</a>
			</div>
			///
		);
	},
	onFocus:function(){
		VotingActions.searchBoxFocused(true);
	},
	onBlur:function(){
		VotingActions.searchBoxFocused(false);
	},
	clearInputField:function(){
		var node=ReactDOM.findDOMNode(this.refs.searchInput);
		node.value='';
		node.focus();
		this.onValueChange();
	},
	onValueChange:function(){
		var q=this.refs.searchInput.value;
		if(q==''){
			this.setState({active:false});
		}else{
			this.setState({active:true});
		}
		VotingActions.searchQueryChanged(q);
	}
});

var ProfTile=React.createClass({
	render:function(){
		var plusOneClass="plusOne ";
		if(this.props.myVote==this.props.data.profid){
			plusOneClass+='myVote';
		}
		// <span className="votesCount">{this.props.data.votes}</span>
		var positionStyle={cursor:'pointer',width:'100%',top: this.props.position*48};
		return (
			<div onClick={this.onClick} className="ProfTile" style={positionStyle}>
				<span className={plusOneClass}>&#10004;</span>
				<span>{this.props.data.name} <b className="dept">({this.props.data.dept})</b></span>
			</div>
			///
		);
	},
	onClick:function(){
		if(this.props.myVote==this.props.data.profid){
			VotingActions.voteFor(this.props.data.profid,true);
			return;
		}
		VotingActions.voteFor(this.props.data.profid,false);
	}
});

var MainWrapper=React.createClass({
	getInitialState:function(){
		return {votes:VotingStore.getVotingStats(),myVote:VotingStore.getMyVote(),showAllFlag:VotingStore.getShowAllFlag(),popupOpen:false,totalVotes:0};
	},
	componentDidMount:function(){
		var self=this;
		VotingActions.init();
		VotingStore.addChangeListener(this.onChange);
		this.setState( {loadingToastID:Toaster.make('Loading...',{type:'loading'}) });
		// Toaster.make('This is a live voting.', 5000);
		var b=function(){
			self.setState({innerWidth:window.innerWidth});
		}
		b();
		window.addEventListener('resize',b);
	},
	render:function(){
		var self=this;
		var c=-1;
		var profTiles=this.state.votes.map(function(r){
			c++;
			if(!self.state.showAllFlag && c > 50){
				return;
			}
			return <ProfTile key={r.profid} myVote={self.state.myVote} data={r} position={c} />
			///
		});
		var voteCount=this.state.votes.length;
		var resultStyle={};
		var showAll=null;
		if(!this.state.showAllFlag && voteCount>50){
			resultStyle.height=51 * 48;
			showAll=(
				<div>
					<div className="divider"></div>
					<div onClick={this.showAll} className="showAll">Show all</div>
				</div>///
			);
		}else{
			resultStyle.height=this.state.votes.length * 48;
		}
		var popupStyle={};
		if( this.state.popupOpen == true){
			popupStyle.opacity=1;
			popupStyle.zIndex=50;
		}else{
			popupStyle.opacity=0;
			popupStyle.zIndex=-50;
		}
		var liveOuterStyle={};
		var liveButtonStyle={color:'#00BCD4',float:'left',padding:'12px 24px 12px 0'};
		var liveVoteCount={color:'#00BCD4',padding:'12px 0 12px 24px'};
		if(this.state.innerWidth<600){
			liveOuterStyle.display='block';
			liveButtonStyle.float='';
			liveButtonStyle.display='inline-block';
			liveVoteCount.display='inline-block';
			liveButtonStyle.padding='12px 24px';
			liveVoteCount.padding='12px 24px';
		}else{
			liveButtonStyle.position='absolute';
			liveButtonStyle.left=0;
			liveVoteCount.right=0;
			liveVoteCount.position='absolute';
		}
		// return(
		// 	<div>
		// 		<div className="aboutButtonWrapper">
		// 			<div style={{textAlign:'center',margin:20}}>
		// 						<span>Phase 2 voting starts at 9:00 PM</span>
		// 			</div>
		// 			<span onClick={this.showAboutAward} className="aboutButton">About the award</span>
		// 		</div>
		// 		<div style={popupStyle} className="popup">
		// 			<div className="popup-inner">
		// 				<span onClick={this.hideAboutAward} className="popup-closeButton">
		// 					<Cross active="true" size="40"/>
		// 				</span>
		// 				<div className="popup-innerBox" style={{margin:'50px 20px'}}>
		// 					Institute awards <b>Gopal Das Bhandari Memorial Distinguished Teacher Award 2015</b> is instituted with intent to honor and reward an outstanding teacher annually, from the prospective of the undergraduate batch to faculty members.
		// 					<div>
		// 						<div>The selection procedure will take place in 2 phases.</div>
		// 						<div className="">
		// 							<div><b>Phase 1 voting :</b> till  20:00 09/06/16 and top 5 will be shortlisted from phase 1</div>
		// 							<div><b>Phase 2 voting :</b> from 21:00 09/06/16 to 20:00 10/06/16</div>
		// 						</div>
		// 					</div>
		// 				</div>
		// 			</div>
		// 		</div>
		// 	</div>
		// );
		//////
		return(
			<div>
				<div className="aboutButtonWrapper">
					<span style={liveOuterStyle}>
						<span onClick={this.showLiveToast} style={liveButtonStyle}>
								<span style={{float:'left',padding:5,borderRadius:'50%',display:'inline-block',background:'#00BCD4',margin:5,height:0}}></span>
								<span style={{float:'left'}}>Phase 2 live</span>
						</span>
					</span>
					<span onClick={this.showAboutAward} className="aboutButton">About the award</span>
					<span style={liveOuterStyle}>
						<span style={liveVoteCount}>
							<span style={{float:'left'}}>Phase 2: {this.state.totalVotes} votes</span>
						</span>
					</span>
				</div>
				<div className="outerLayer clearfix">
					<SearchBox/>
					<div className="allTilesWrapper" style={resultStyle}>
						{profTiles}
					</div>
					{showAll}
				</div>
				<div style={popupStyle} className="popup">
					<div className="popup-inner">
						<span onClick={this.hideAboutAward} className="popup-closeButton">
							<Cross active="true" size="40"/>
						</span>
						<div className="popup-innerBox" style={{margin:'50px 20px'}}>
							Institute awards <b>Gopal Das Bhandari Memorial Distinguished Teacher Award 2015</b> is instituted with intent to honor and reward an outstanding teacher annually, from the prospective of the undergraduate batch to faculty members.
							<div>
								<div>The selection procedure will take place in 2 phases.</div>
								<div className="">
									<div><b>Phase 1 voting :</b> till  20:00 09/06/16 and top 5 will be shortlisted from phase 1</div>
									<div><b>Phase 2 voting :</b> from 21:00 09/06/16 to 20:00 10/06/16</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
		///
	},
	onChange:function(){
		this.setState({
			votes:VotingStore.getVotingStats(),
			myVote:VotingStore.getMyVote(),
			showAllFlag:VotingStore.getShowAllFlag(),
			totalVotes:VotingStore.getTotalVotes(),
		});
		
		Toaster.clear(this.state.loadingToastID);
		
		clearTimeout(this.state.timeouts);
		
		var timeoutid=setTimeout(function(){
			VotingActions.updateStats();
		},VotingStore.getTimeLag()*1000);
		this.setState({timeouts:timeoutid});
	},
	showAll:function(){
		VotingActions.updateShowAllFlag(true);
	},
	showAboutAward:function(){
		this.setState({popupOpen:true});
	},
	hideAboutAward:function(){
		this.setState({popupOpen:false});
	},
	showLiveToast:function(){
		// Toaster.make('This is a live voting.', 5000);
	}
});
module.exports=MainWrapper;