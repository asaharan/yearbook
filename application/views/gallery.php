<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=460, initial-scale=1.0"/>
	<title>Gallery - Yearbook</title>
	<base href="<?php echo base_url(); ?>"></base>
	<style type="text/css">
		body{
			margin: 0;
			background-color: #fafafa;
			color: #333;
		}
		*,*:after,*:before{
			box-sizing: border-box;
			font-family: sans-serif;
		}
		.file-input{
			height: 0;
			margin: 0;
			opacity: 0;
			padding: 0;
			width: 0;
		}
		.upload-button-container{
			margin: 10px auto 0px;
			text-align: center;
		}
		.upload-button{
			background-color: #0095FF;
			border: none;
			border-radius: 3px;
			box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.1);
			color: #fff;
			cursor: pointer;
			font-family: sans-serif;
			outline: none;
			overflow: hidden;
			padding: 10px 24px;
			position: relative;
			width: 100%;
		}
		.upload-button:focus{
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.075), 0 0 5px rgba(81, 167, 232, 0.5);
		}
		.file-select-button{
			background: #fafafa;
			border: 1px solid #f0f0f0;
			border-radius: 3px;
			color: #313131;
			cursor: pointer;
			display: block;
			margin: 0 auto;
			padding: 8px 10px;
			text-align: center;
			outline: none;
		}
		.file-select-button-container[data-focus=true] .file-select-button{
			background-color: #fff;
			border: 1px solid #51a7e8;
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.075), 0 0 5px rgba(81, 167, 232, 0.5);
		}
		.popup-material{
			position: fixed;
			width: 100%;
			height: 100%;
			top: 0;
			left: 0;
			transition: background-color ease 300ms,opacity ease 300ms;
			z-index: 5;
			overflow-y: auto;
		}
		.popup-material-inner{
			position: relative;
		    display: flex;
		    justify-content: center;
		    align-items: center;
		    min-height: 100vh;
		}
		.inner-box{
			min-width: 440px;
			max-width: 600px;
			margin: 20px auto;
			background-color: #fff;
			border-radius: 1px;
			background-color: #fff;
			border: 1px solid #f8f8f8;
		}
		.upload-form{
			padding: 12px;
		}
		.brand{
			color: #0095FF;
			font-size: 30px;
			margin: 12px 0;
			text-align: center;
		}
		.tagline{
			text-align: center;
			margin-bottom: 12px;
		}
		.input-field{
			border: 1px solid #f0f0f0;
			padding: 8px 12px;
			display: block;
			width: 100%;
			border-radius: 3px;
			outline: none;
		}
		.input-field:focus{
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.075), 0 0 5px rgba(81, 167, 232, 0.5);
			border-color: #51a7e8;
		}
		label{
			display: block;
			margin-bottom: 8px;
			font-size: 14px;
		}
		legend{
			margin-bottom: 4px;
		}
		.divider{
			height: 1px;
			background-color: #f8f8f8;
		}
		.selected-file-options{
			border: solid 1px #f8f8f8;
			border-radius: 3px;
			outline: none;
			background-color: #fafafa;	
		}
		.file-select-button-container[data-focus=true] .selected-file-options,.file-select-button-container[data-focus=true] .selected-file-option-title{
			background-color: #fff;
			border-color: #51a7e8;
			box-shadow: inset 0 1px 2px rgba(0, 0, 0, 0.075), 0 0 5px rgba(81, 167, 232, 0.5);
		}
		.selected-file-option-title{
			display: inline-block;
			border-right: solid 1px #f8f8f8;
			padding: 8px 12px;
		}
		.gallery{
			max-width: 800px;
			margin: 0 auto;
		}
		.gallery .image{
			max-width: 400px;
			max-height: 200px;
			padding: 0;
			overflow: hidden;
			display: inline-block;
			float: left;
		}
		.gallery .image img{
			float: left;
			width: 380px;
		}
		.image-wrapper{
			position: relative;
			display: inline-block;
			margin: 10px;
		}
		.delete-button{
			opacity: 0;
			position: absolute;
			bottom: 10px;
			left: 100px;
			padding: 7px 12px;
			border-radius: 3px;
			text-align: center;
			width: 200px;
			background: rgba(244, 67, 54, 0.75);
			color: #fff;
			font-weight: bold;
			z-index: 5;
			transition: all ease 400ms;
			cursor: pointer;
			text-decoration: none;
			transform: scale(0.5);
		}
		.image-wrapper:hover .delete-button{
			transform: none;
			opacity: 1;
		}
		.delete-button:hover{
			transform: none;
			background-color: rgba(244, 67, 54, 0.98);
		}
	</style>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="e/css/main.css">
</head>
<body>
<?php 
	$this->view('navbar');
?>
<div style="line-height: 56px;">&nbsp;</div>
<div class="inner-box">
	<form onsubmit="return (_onSubmitMainForm(event));" class="upload-form" action="<?php echo base_url(); ?>upload/u/do_upload" enctype="multipart/form-data" method="post" accept-charset="utf-8">
		<label>
			<div id="fileSelectButtonContainer" class="file-select-button-container">
				<span id="fileSelectButton" class="file-select-button">Select the file to share</span>
				<div id="selectedFileOptions" class="selected-file-options"style="display: none">
					<span class="selected-file-option-title">File</span>
					<span id="selectedFilename" class="selected-file-option-name">filename.zip</span>
				</div>
			</div>
			<input type="file" accept="*" tabindex="1" name="userfile" id="fileInputNode" class="file-input">
		</label>
		<div class="upload-button-container">
			<button type="submit" class="upload-button">Upload File</button>
		</div>
	</form>
</div>
	<div style="text-align: center;margin-bottom: 10px;padding: 5px 20px;background: #fff;max-width: 600px;margin: 0 auto 12px;">
		<?php
			if(sizeof($data)==0){
				echo "You have not uploaded any images yet";
			}else{
				echo "Your uploaded images";
			}
		?>
	</div>
	<div class="gallery">
		<?php
			for($i=0;$i<sizeof($data);$i++){
				echo "<div class='image-wrapper'><a href='".$data[$i]['path']."' class='image'><img src='".$data[$i]['path']."'/></a>";
				echo "<a href='".base_url()."upload/u/delete?i=".$data[$i]['id']."' class='delete-button'>Delete</a>";
				echo "</div>";
			}
		?>
	</div>
	<script type="text/javascript">
		var fileInputNode=document.getElementById('fileInputNode');
		var selectedFileOptions=document.getElementById('selectedFileOptions');
		var fileSelectButton=document.getElementById('fileSelectButton');
		var selectedFilename=document.getElementById('selectedFilename');
		var fileSelectButtonContainer=document.getElementById('fileSelectButtonContainer');
		var selectedFile={};
		fileInputNode.addEventListener('change',function(e){
			selectedFile.path=e.target.value;
			var s=selectedFile.path.split('\\');
			selectedFile.name=s[s.length - 1];
			if(s==''||s==undefined||s==null){
				_onFileUnselect()
			}else{
				_onFileSelect();
			}
		});
		function _onFileSelect(){
			fileSelectButton.setAttribute('style','display:none');
			selectedFileOptions.setAttribute('style','');
			selectedFilename.innerText=selectedFile.name;
			selectedFileOptions.focus();
		}
		function _onFileUnselect(){
			fileSelectButton.setAttribute('style','');
			selectedFileOptions.setAttribute('style','display:none');
			fileSelectButton.focus();
		}
		function _onFileInputNodeFocus(){
			fileSelectButtonContainer.setAttribute('data-focus','true');
		}
		function _onFileInputNodeBlur(){
			fileSelectButtonContainer.setAttribute('data-focus','false');
		}
		fileInputNode.addEventListener('focus',_onFileInputNodeFocus);
		fileInputNode.addEventListener('blur',_onFileInputNodeBlur);
	</script>
	<script type="text/javascript">
		window.addEventListener('load',function(){
			console.log('yo');
		});
	</script>