<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BestProfessor extends CI_Controller {
	public function index(){
		$this->load->view('Votes');
	}
	public function vote(){
		$profid=$this->input->get('profid');
		$clear_vote=$this->input->get('clear_vote');
		$response=[];
		$response['success']=FALSE;
		if(empty($profid)){
			echo json_encode($response);
			return;
		}
		if(empty($clear_vote)){
			$clear_vote=FALSE;
		}else{
			$clear_vote=TRUE;
		}
		$this->load->helper('login');
		if(!isloggedin()){
			echo json_encode($response);
			return;
		}
		$userid=userid();
		$this->db->where(array('userid'=>$userid));
		$this->db->update('user_votes',array('deleted'=>1));
		
		$response['success']=true;
		if($clear_vote){
			echo json_encode($response);
			return;
		}
		//try to update previous vote
		$this->db->where(array('userid'=>$userid));
		$result=$this->db->update('user_votes',array('deleted'=>0,'profid'=>$profid));
		$affected_rows=$this->db->affected_rows();
		if($affected_rows==0){
			$this->db->insert('user_votes',array('userid'=>$userid,'profid'=>$profid));
		}
		echo json_encode($response);
	}
	public function stats(){
		$response=[];
		$after=$this->input->get('after');
		
		$this->db->select('count(distinct userid) as total_votes');
		$result=$this->db->get_where('user_votes',array('deleted'=>0))->result_array();
		$response['total_votes']=$result[0]['total_votes'];

		$this->load->helper('login');
		$userid=1;
		if(isloggedin()){
			$userid=userid();
			$result=$this->db->get_where('user_votes',array('userid'=>$userid,'deleted'=>0))->result_array();
			if(sizeof($result)>0){
				$response['my_vote']=$result[0]['profid'];
			}
		}		

		if(!empty($after)){
			$this->db->where('timestamp > ',$after);
		}
		$this->db->select('max(timestamp) as last_time');
		$result=$this->db->get('user_votes')->result_array();
		$last_time=$after;
		if(sizeof($result)==1 && $result[0]['last_time']==NULL){
			$response['update']=FALSE;
			$response['last_time']=$after;
			echo json_encode($response);
			return;
		}else{
			$last_time=$result[0]['last_time'];
		}

		if(!empty($after)){
			if(isloggedin()){
				$this->db->where(array('userid!='=>$userid));
			}
			$this->db->join('profs','profs.id=user_votes.profid');
			$this->db->select('count(distinct userid) as votes, profid, profs.name, profs.dept');
			$this->db->order_by('votes desc');
			$this->db->group_by('profs.id');
			$this->db->limit(3);
			$result=$this->db->get_where('user_votes',array('deleted'=>0,'timestamp>'=>$after))->result_array();
			if(sizeof($result)>0 ){
				$response['newVotes']=$result;
			}else{
				$response['newVotes']=null;
			}
		}else{
			$response['newVotes']=null;	
		}

		$sql = "select a.id as profid, a.name, a.dept, b.votes from profs as a left join\n"."(SELECT profid, count(distinct userid) as votes from user_votes where deleted=0 group by profid ) b on a.id=b.profid order by b.votes desc, a.name asc";
		$result=$this->db->query($sql)->result_array();
		
		$response['data']=$result;
		if(sizeof($result)==0){
			$response['update']=FALSE;
			echo json_encode($response);
			return;
		}
		$response['update']=TRUE;
		$response['last_time']=$last_time;

		//if user is loggedin then send his voteId too
		echo json_encode($response);
		return;
	}
}