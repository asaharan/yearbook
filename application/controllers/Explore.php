<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Explore extends CI_Controller {
	public function index()
	{
		$this->load->helper('login');
		if(isloggedin()||TRUE){
			$this->load->view('explore');
		}else{
			redirect('/','refresh');
		}
	}
	public function search(){
		$r=[];
		$r['success']=false;
		$q=$this->input->post('q');
		if(empty($q)){
			echo json_encode($r);
			return;
		}

		$this->load->helper('login');
		$userid=isloggedin()?userid():0;
		$this->db->select('name,username,avatar,department,fondly_known_for as known_for');
		$this->db->group_start();
		$this->db->like('name',$q,'both');
		$this->db->or_like('username',$q,'both');
		$this->db->group_end();
		$this->db->where_not_in('id',array($userid));
		$x=$this->db->get_where('users',array('confirm'=>1))->result_array();
		// $r['query']=$this->db->last_query();
		if(sizeof($r)==0){
			echo json_encode($r);
			return;
		}
		$r['success']=TRUE;
		$r['data']=$x;
		echo json_encode($r);
	}
	public function test(){
		$this->load->helper('html_purifier');
		echo html_purify('<div>hello</div><script>alert()</script>');
	}
}