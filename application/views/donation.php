<!DOCTYPE html>
<html>
<head>
	<title>Donation</title>
	<style type="text/css">
		*,*:after,*:before{
			font-family: Roboto;
			box-sizing: border-box;
		}
		body{
			background: #fff;
			margin: 0;
		}
		.donation-tab{
			position: relative;
			display: block;
			text-align: center;
		}
		.donation-tab .tab{
			display: inline-block;
			padding: 0 20px;
			line-height: 42px;
			border:none;
			background: #8f7a66;
			color: #f9f6f2;
			border-radius: 3px;
			font-weight: 400;
			margin: 6px;
			cursor: pointer;
			transition: all ease 400ms;
			font-size: 18px;
			background: #1067AC;
		}
		.donation-tab .tab.active{
			background: #483B2E;
			color: #fff;
			background: #114978;
		}
		.donation-tab .tab.cancel{
			background: #9E9286;
			display: none;
			background: #aaa;
		}
		.reason{
			max-width: 800px;
			display: block;
			margin: 6px auto;
			background: #f9f9f9;
			border-radius: 3px;
			padding: 12px;
		}
		.reason div{
			margin-bottom: 12px;
			color: #313131;
		}
	</style>
</head>
<body style="background:#fff">
	<img alt="Donate for Senior Classroom Gift" src="<?php echo base_url(); ?>e/img/donate_now.jpg" style="width:450px;display:block;margin: 12px auto;">
	<div class="donation-tab">
		<div data-amount="2000" class="tab">
			<span>2000</span>
		</div>
		<div data-amount="3000" class="tab">
			<span>3000</span>
		</div>
		<div data-amount="4000" class="tab">
			<span>4000</span>
		</div>
		<div data-amount="4800" class="tab">
			<span>4800</span>
		</div>
		<div data-amount="-1" class="tab cancel">
			<span>Cancel</span>
		</div>
	</div>
	<div class="reason">
		<div>Oxidation Pond, located near the Festival grounds has huge potential to be developed into a recreational place. With the money received from the Senior Classroom gift program, the place would undergo complete renovation with construction of Sitting space, setting up of Garden lights, planting trees and other additions that may help with the beautification. If sufficient money is received, we will also renovate the dry river channel running parallel to the Hockey Ground and the road connecting Hall 9 and Hall 7. So donate in large numbers!</div>

		<div><b>P.S:</b> The money would be deducted from your security deposit. So, just click on the desired option and help us with the renovation!&nbsp;&nbsp;&nbsp;The names of all the donators would engraved on the benches.</div>
	</div>
	<script type="text/javascript">
		var base_url='<?php echo base_url(); ?>';
		var donation_amount=<?php echo $donation_amount; ?>;
	</script>
	<script type="text/javascript">
	function loadjscssfile(t,e,i){if("js"==e){var s=document.createElement("script");s.setAttribute("type","text/javascript"),s.setAttribute("src",t)}else if("css"==e){var s=document.createElement("link");s.setAttribute("rel","stylesheet"),s.setAttribute("type","text/css"),s.setAttribute("href",t)}i&&s.addEventListener("load",function(t){i(null,t)}),"undefined"!=typeof s&&document.getElementsByTagName("head")[0].appendChild(s)}
		window.addEventListener('load',function(){
			loadjscssfile('e/js/jquery-2.1.1.min.js','js',function(){
				init();
			});
		});
		var init=function(){
			$('.tab[data-amount='+donation_amount+']').addClass('active');
			if( donation_amount > 0){
				$('.cancel').css({'display':'inline-block'});
			}
			$('.tab').on('click',function(){
				var _this=this;
				var amount=$(this).attr('data-amount');
				$.ajax({
					method:'POST',
					url:base_url+'home/donate',
					data:{amount:amount},
					dataType:'json',
					success:function(r){
						loadjscssfile(base_url+'e/css/main.css','css');
						loadjscssfile(base_url+'e/js/os/main.js','js',function(){
							Materialize.toast(r.reason,5000);
							if(r.success&&amount>0){
								$('.cancel').css({'display':'inline-block'});
								$('.active').removeClass('active');
								$(_this).addClass('active');
							}else{
								$('.active').removeClass('active');
								$('.cancel').css({'display':'none'});
							}
						});
					}
				})
			});
			loadjscssfile('https://fonts.googleapis.com/css?family=Roboto','css');
		}
	</script>
</body>
</html>