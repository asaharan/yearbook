<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Donate extends CI_Controller {
	public function index()
	{
		$this->load->helper('login');
		$r=[];
		$r['success']=FALSE;
		$userid=userid();
		if(!isloggedin()){
			$userid=0;
		}
		$this->db->select('donation_amount');
		$r=$this->db->get_where('users',array('id'=>$userid))->result_array();
		if(sizeof($r)>0){
			$donation_amount=$r[0]['donation_amount'];
		}
		$this->load->view('donation',array('donation_amount'=>$donation_amount));
	}
}