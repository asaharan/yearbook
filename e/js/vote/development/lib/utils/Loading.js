var React = require('react');
var Style={
	border: '2px solid #fff',
	borderTopColor: 'transparent',
	borderRadius:'50%',
	WebkitAnimation:'toastSpin 1.1s infinite linear',
	animation:'toastSpin 1.1s infinite linear',
	display:'inline-block',
	height:16,
	marginRight: 12,
	verticalAlign: 'middle',
	width: 16
}
function addStyleSheet(){
	var s=document.createElement('style');
	// console.log(s);
	s.innerHTML='@-webkit-keyframes toastSpin {from {-webkit-transform: rotate(0); }to {-webkit-transform: rotate(360deg); } }@-moz-keyframes toastSpin {from {-moz-transform: rotate(0); }to {-moz-transform: rotate(360deg); } }@keyframes toastSpin {from {-webkit-transform: rotate(0);-moz-transform: rotate(0);-ms-transform: rotate(0);-o-transform: rotate(0);transform: rotate(0); }to {-webkit-transform: rotate(360deg);-moz-transform: rotate(360deg);-ms-transform: rotate(360deg);-o-transform: rotate(360deg);transform: rotate(360deg); } }';
	document.getElementsByTagName('head')[0].appendChild(s);
}
var stylesheetAdded=false;
var Loading = React.createClass({
	render:function(){
		var s=Style;
		if(!stylesheetAdded){
			addStyleSheet();
		}
		if(this.props.data){
			if(this.props.data.style){
				for(p in this.props.data.style){
					s[p]=this.props.data.style[p];
				}
			}
		}
		stylesheetAdded=true;
		return(
			<span style={s}></span>
			/**/
		);
	}
});
module.exports= Loading;