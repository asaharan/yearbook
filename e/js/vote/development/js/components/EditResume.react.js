var React = require('react');
var $ = require('jquery');
var makeTree=require('../lib/makeTree');
var MainWrapper=require('./MainWrapper.react');
var EditResume = React.createClass({
	getInitialState:function(){
		return {tree:[]};
	},
	componentDidMount:function(){
		var resumeId=this.props.params.resumeId;
		var self=this;
		$.ajax({
			method:'GET',
			url:'api/resume',
			data:{id:resumeId},
			success:function(r){
				if(r.success==true){
					var tree=makeTree(r.data);
					self.setState({tree:tree,success:r.success});
				}
			}
		})
	},
	render:function(){
		var newSection=(
			<div>
				<input type="text" ref="newSection"/>
				<button>Add Section</button>
			</div>
		);
		var InputFields=(
			<div>
				<div></div>
				<div>{newSection}</div>
			</div>
		);
		return (
			<div>
				<div>Editing Resume#{this.props.params.resumeId}</div>
				<div>
					{InputFields}
				</div>
				<MainWrapper params={{resumeId:this.props.params.resumeId}}/>
			</div>
		)
			/**/
	}
});
module.exports=EditResume;