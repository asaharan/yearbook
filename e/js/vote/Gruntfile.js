module.exports = function(grunt) {

  grunt.initConfig({
    uglify:{
      build:{
        src:'public/js/bundle.js',
        dest:'public/js/bundle.min.js'
      }
    },
    browserify: {
      dist:{
        files: {
          'public/js/bundle.js': ['development/js/main.js']
        },
        options: {
          transform:[["babelify",{presets:"react"}]],
        }
      }
    },
    sass:{
      dist:{
        options:{
          style:'compressed',
          sourcemap:'none'
        },
        files:{
          'public/css/main.css':'development/styles/main.scss'
        }
      }
    },
    connect: {
      server:{
        options:{
          port:3489
        }
      }
    },
    watch: {
      bundle:{
        files: ['public/js/bundle.js'],
        tasks: ['uglify']
      },
      scripts:{
        files: ['development/js/*.js','development/js/*/*.js'],
        tasks: ['browserify']
      },
      styles:{
        files:['development/styles/*.scss','development/styles/lib/*.scss'],
        tasks:['sass']
      }
    }
  });

  // bundle:{
  //   files: ['public/js/bundle.js'],
  //   tasks: ['uglify']
  // },
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  // grunt.loadNpmTasks('grunt-contrib-connect');

  grunt.registerTask('default', ['watch']);

};
