<!DOCTYPE html>
<html>
	<head>
		<title>Confirmation of email</title>
	</head>
	<body style="text-align:center">
		<div style="margin:5px;font-size:20px;">Your email has been confirmed successfully.</div>
		<div style="font-size:18px;">Visit <a href="<?php echo base_url(); ?>">Yearbook homepage</a> to explore more.</div>
	</body>
</html>