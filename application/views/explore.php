<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=460, initial-scale=1.0"/>
	<title>Find your friends</title>
	<base href="<?php echo base_url(); ?>"></base>
	<link rel="stylesheet" type="text/css" href="e/css/materialize/css/materialize.css">
	<link rel="stylesheet" type="text/css" href="e/css/main.css">
</head>
<body>
<div class="container" style="max-width: 600px;">
	<?php $this->view('navbar');?>
	<div class="search-box">
		<input placeholder="Search" autofocus="true" type="text" class="search-input" style="box-shadow: none;" />
	</div>
	<div id="searchResults"></div>
	<script type="text/javascript">
		var base_url='<?php echo base_url(); ?>';
	</script>
<script type="text/javascript" src="e/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="e/js/os/main.js"></script>
<script type="text/javascript">
	var searchResults=$('#searchResults');
	var pv='';

	function searchTile(data){
		var a=$('<div>');
		a.addClass('search-tile clearfix medium-profile-link');
		var b = $('<img>');
		b.addClass('avatar square-avatar medium-avatar profile-image');
		b.attr('src',data.avatar);
		var d = $('<a>');
		d.attr('href',base_url+'profile/'+data.username);
		d.append(b);
		d.addClass('clearfix float-left');
		
		var e=$('<div>');
		e.addClass('float-left');
		e.css({'line-height':'normal'});
		var c = $('<a>');
		c.attr('href',base_url+'profile/'+data.username);
		c.text(data.name);
		c.addClass('search-person-property');
		e.append(c);
		var f = $('<div>').addClass('search-person-property').html(data.known_for);
		e.append(f);

		if(data.department!=''){
			f=$('<div>').addClass('search-person-property').html(data.department);
			e.append(f);
		}


		a.append(d);
		a.append(e);
		return a;
	}
	var last_search_id=0;
	function search(q){
		var this_search_id = last_search_id+1;
		last_search_id++;
		$.ajax({
			url:base_url + 'explore/search',
			method:'POST',
			data:{q:q},
			dataType:'json',
			success:function(r){
				if(last_search_id > this_search_id){
					return;
				}
				if(r.success){
					searchResults.html('');
					for(var i=0;i<r.data.length;i++){
						searchResults.append(searchTile(r.data[i]));
					}
				}
			}
		});
	}

	function handleSearchKeys(){
		var v=$('.search-input').val().trim();
		if(v==pv){return false}
		search(v);
		pv=v;
	}
	$('.search-input').on('keyup',handleSearchKeys);
	handleSearchKeys();
</script>